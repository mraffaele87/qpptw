package se.dataductus.postpac.warehouse.solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import se.dataductus.postpac.warehouse.domain.Resource;

/**wraps up the items to get for a specific partial order*/
public class ItemsToGetForOrder {
	private final List<ItemToGet> itemsToGet;
	private final Set<Resource> locations;
	private final int orderID;
	private final int quantity;
	
	/**@throws IllegalArgumentException if any of the items to get are from different orders, or if no items are specified*/
	public ItemsToGetForOrder(List<ItemToGet> itemsToGet) {
		if (itemsToGet.isEmpty()) {
			throw new IllegalArgumentException("itemsToGet is empty!");
		}
		
		this.orderID = itemsToGet.get(0).getOrderID();

		int quantity = 0;
		this.locations = new HashSet<>();
		for (ItemToGet itemToGet : itemsToGet) {
			if (itemToGet.getOrderID() != orderID) {
				throw new IllegalArgumentException("Items must all be from the same order! Found at least " + itemToGet.getOrderID() + " and " + orderID);
			}
			
			quantity += itemToGet.getQuantity();
			this.locations.add(itemToGet.getLocation());
		}
		
		this.itemsToGet = Collections.unmodifiableList(new ArrayList<>(itemsToGet));
		this.quantity = quantity;
	}
	
	public List<ItemToGet> getItemsToGet() {
		return Collections.unmodifiableList(itemsToGet);
	}
	
	public int getOrderID() {
		return orderID;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public Set<Resource> getLocations() {
		return Collections.unmodifiableSet(locations);
	}
}
