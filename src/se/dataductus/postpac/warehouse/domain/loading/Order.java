package se.dataductus.postpac.warehouse.domain.loading;

import java.util.List;

public class Order {
	private final int id;
	private final List<OrderItem> items;
	
	public Order(int id, List<OrderItem> itemIDs){
		this.id = id;
		this.items = itemIDs;
	}
	
	/**
	 * get a list of items and their quantities required by this order
	 * there will not be more than one entry in this list for the same Item
	 */
	public List<OrderItem> getItems() {
		return items;
	}
	
	public int getId() {
		return id;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		boolean first = true;
		for (OrderItem item : this.items) {
			buf.append(first ? "" : ", ");
			buf.append(item);
			first = false;
		}
		
		return "Order[id="+this.id+"," + buf.toString() + "]";
	}
}
