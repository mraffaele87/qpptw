package se.dataductus.postpac.warehouse.domain.loading;

import java.util.List;

public class NodeVisit {
	private int nodeID;
	
	/**time of arrival at node*/
	private double startTime;
	
	/**time of exit from node*/
	private double endTime;
	
	/**empty if just passing through, otherwise this specifies the list of things to pick up*/
	private List<ItemToGet> items;
	
	public NodeVisit(int nodeID, double startTime) {
		this(nodeID, startTime, startTime, null);
	}
	
	public NodeVisit(int nodeID, double startTime, double endTime, List<ItemToGet> items) {
		this.nodeID = nodeID;
		this.startTime = startTime;
		this.endTime = endTime;
		this.items = items;
	}
	
	public int getNodeID() {
		return nodeID;
	}
	
	public double getStartTime() {
		return startTime;
	}
	
	public double getEndTime() {
		return endTime;
	}
	
	public void setEndTime(double endTime) {
		this.endTime = endTime;
	}
	
	public List<ItemToGet> getItems() {
		return items;
	}
	
	public void setItems(List<ItemToGet> items) {
		this.items = items;
	}
	
	@Override
	public String toString() {
		return "NV[" + this.nodeID + "," + this.startTime + "-" + this.endTime + "]";
	}
}