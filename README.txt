Datasets for the paper "Conflict-free routing of multi-stop warehouse trucks"
by Alexander E.I. Brownlee, Jerry Swan, Richard Senington and Zoltan A. Kocsis

src - Source Code used in the paper, including the revised implementation of QPPTW. Mixture of Scala and Java. (compile and run with scala 2.11.11 and java 8)

lib - The Haiku library required by the code (see: Kocsis, Z., Brownlee, A., Swan, J., Senington, R. Haiku - a Scala combinator toolkit for semi-automated composition of metaheuristics. Proc. SSBSE 2015, Bergamo, Italy. LNCS 9275. pp. 125-140. Springer. DOI:10.1007/978-3-319-22183-0_9);  also required are gson 2.2.4, jgrapht 0.8.3 and jgraphx 2.3.0.5 (not included)

resources - The problem definition that we worked with, in JSON format

results - raw data from which the aggregated results in the paper were drawn
