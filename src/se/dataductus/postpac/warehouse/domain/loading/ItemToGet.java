package se.dataductus.postpac.warehouse.domain.loading;


/**A specific item for a truck to get, with the location it'll be got from (and the original order being fulfilled)*/
public class ItemToGet {
	private final int itemID;
	private final int locationID;
	private final int quantity;
	private final int orderID;
	
	public ItemToGet(int itemID, int locationID, int quantity, int orderID) {
		this.itemID = itemID;
		this.locationID = locationID;
		this.quantity = quantity;
		this.orderID = orderID;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public int getLocationID() {
		return locationID;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public int getOrderID() {
		return orderID;
	}
	
	@Override
	public String toString() {
		return "ItemToGet[item=" + itemID + ", location=" + locationID + ", qty=" + quantity + ", orderID=" + orderID + " ]";
	}
}
