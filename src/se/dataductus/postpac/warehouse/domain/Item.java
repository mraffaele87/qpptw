package se.dataductus.postpac.warehouse.domain;

import java.util.ArrayList;
import java.util.List;

public class Item {
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		boolean first = true;
		for (Resource location : locations) {
			buf.append(first ? "" : ",");
			buf.append(location.getLocation());
			buf.append("=");
			buf.append(location.getQuantity());
		}
		
		return "Item[" + (name != null ? "name=" + name + ", ": "") + "locations=[" + buf.toString() + "] ]";
	}
	
	public String toShortString() {
		return "Item[" + (name != null ? "name=" + name + ", ": "") + "id=" + id + "]";
	}

	private final String name;
	private final int id;
	private final List<Resource> locations;
	
	public Item(int id,String name){
		locations = new ArrayList<>();
		this.id = id;
		this.name = name;
	}
	
	public Item(int id){
		locations = new ArrayList<>();
		this.id = id;
		this.name = null;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object that) {
		return (that instanceof Item) && (this.id == ((Item)that).id);
	}
	
	@Override
	public int hashCode() {
		return this.id;
	}

	public List<Resource> getLocations() {
		return locations;
	}
}
