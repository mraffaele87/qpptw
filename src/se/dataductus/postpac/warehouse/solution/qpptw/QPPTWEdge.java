package se.dataductus.postpac.warehouse.solution.qpptw;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import org.jgrapht.graph.DefaultWeightedEdge;

import se.dataductus.postpac.warehouse.domain.Truck;

public class QPPTWEdge extends DefaultWeightedEdge {
	private static final long serialVersionUID = 2880487024434848442L;
	
	private final se.dataductus.postpac.warehouse.domain.Edge underlyingEdge;
	private final QPPTWNode nodeFrom;
	private final QPPTWNode nodeTo;
	private NavigableSet<TimeWindow> setOfSortedTimeWindows;
	private List<Reservation> reservations;
	
	/**
	 * the set of edges that are close enough to this one that they will be blocked when this edge is occupied - also includes this edge
	 * but remains empty until determineConflictingEdges() is called
	 */
	private Set<QPPTWEdge> conflictingEdges;
	
	/**minimum time in milliseconds required to traverse this edge - time-windows shorter than this are typically disallowed*/
	private double minTaxiTime;
	
	public QPPTWEdge(se.dataductus.postpac.warehouse.domain.Edge underlyingEdge, QPPTWNode nodeFrom, QPPTWNode nodeTo) {
		this.underlyingEdge = underlyingEdge;
		this.nodeFrom = nodeFrom;
		this.nodeTo = nodeTo;
		this.setOfSortedTimeWindows = new TreeSet<>(new TimeWindow.StartAndEndTimeComparator()); // time windows won't overlap, so we use this to allow comparisons with other time periods
		this.conflictingEdges = new HashSet<>();
		this.minTaxiTime = 0; // initially no limit
		this.reservations = new ArrayList<>();
	}
	
	public void setInitialTimePeriod(double startTime, double endTime) {
		this.setOfSortedTimeWindows.clear();
		this.setOfSortedTimeWindows.add(new TimeWindow(startTime, endTime));
	}

	public QPPTWNode getNodeFrom() {
		return nodeFrom;
	}

	public QPPTWNode getNodeTo() {
		return nodeTo;
	}
	
	public double getLength() {
		return this.underlyingEdge.getLength();
	}

	public se.dataductus.postpac.warehouse.domain.Edge getUnderlyingEdge() {
		return underlyingEdge;
	}
	
	public void setMinTaxiTime(double minTaxiTime) {
		this.minTaxiTime = minTaxiTime;
	}
	
	public double getMinTaxiTime() {
		return minTaxiTime;
	}
	
	public void determineConflictingEdges(Collection<QPPTWEdge> allEdges, double thresholdDistanceInMetres) {
		this.conflictingEdges.add(this);
		for (QPPTWEdge edge : allEdges) {
			if (edge != this) {
				// if edges adjacent, they are conflicting
				// if edges intersect, they are conflicting
				// otherwise, if the edges are less than the threshold apart, they also conflict
				if (edge.isAdjacentTo(this) || (edge.intersects(this)) || (edge.distanceFrom(this) < thresholdDistanceInMetres)) {
					this.conflictingEdges.add(edge);
				}
			}
		}
	}
	
	/**for convenience, conflicting Edges also contains this edge*/
	public Set<QPPTWEdge> getConflictingEdges() {
		return conflictingEdges;
	}
	
	public boolean conflictsWith(QPPTWEdge that) {
		return this.conflictingEdges.contains(that);
	}
	
	public NavigableSet<TimeWindow> getSetOfSortedTimeWindows() {
		return setOfSortedTimeWindows;
	}
	
	public boolean isAdjacentTo(QPPTWEdge te) {
		return containsNode(te.getNodeFrom()) || containsNode(te.getNodeTo());
	}
	
	public double distanceFrom(QPPTWEdge that) {
		// take the four end points, and calc distance from each to the nearest point on the other edge
		//double distance1 = this.nearestPointOnLine(that.getNodeA().getLatLonCoordinate(), true)[2];
		//double distance2 = this.nearestPointOnLine(that.getNodeB().getLatLonCoordinate(), true)[2];
		//double distance3 = that.nearestPointOnLine(this.getNodeA().getLatLonCoordinate(), true)[2];
		//double distance4 = that.nearestPointOnLine(this.getNodeB().getLatLonCoordinate(), true)[2];
		//
		//return ArrayTools.min(new double[] {distance1, distance2, distance3, distance4});
		
		// for now, we'll say that all edges are separate except immediately neighbouring ones
		// because we don't have xy coords to measure
		if (this.containsNode(that.getNodeFrom()) || this.containsNode(that.getNodeTo())) {
			return 0;
		} else {
			return Double.POSITIVE_INFINITY;
		}
	}
	
	/**uses x/y coords rather than lat/lon as it's 2D geometry*/
	public boolean intersects(QPPTWEdge that) {
		double x1 = this.nodeFrom.getxCoordinate();
		double y1 = this.nodeFrom.getyCoordinate();
		double x2 = this.nodeTo.getxCoordinate();
		double y2 = this.nodeTo.getyCoordinate();
		double x3 = that.nodeFrom.getxCoordinate();
		double y3 = that.nodeFrom.getyCoordinate();
		double x4 = that.nodeTo.getxCoordinate();
		double y4 = that.nodeTo.getyCoordinate();
		
		// make equations
		double a1 = (y2-y1)/(x2-x1);
		double b1 = y1 - a1*x1; 
		double a2 = (y4-y3)/(x4-x3);
		double b2 = y3 - a2*x3;
		
    	// line parallel? check intercept and if that's equal, lines overlap as long as x1,x2 and x3,x4 overlap (ie x3 or x4 is between x1&x2)
		if ((a1 == a2) && (b1 == b2)) {
			double min = Math.min(x1, x2);
			double max = Math.max(x1, x2);
			return ((x3 > min) && (x3 < max)) || ((x4 > min) && (x4 < max));
		} else { // lines not parallel. figure out where they intersect, then check that lies between x1,x2 and x3,x4 
			double x0 = -(b1-b2)/(a1-a2);
			
			double min1 = Math.min(x1, x2);
			double max1 = Math.max(x1, x2);
			double min2 = Math.min(x3, x4);
			double max2 = Math.max(x3, x4);
			
			return (x0 > min1) && (x0 < max1) && (x0 > min2) && (x0 < max2);
		}
	}
	
	public boolean containsNode(QPPTWNode tn) {
		return (this.nodeFrom == tn) || (this.nodeTo == tn);
	}
	
	@Override
	public boolean equals(Object that) {
		return this.underlyingEdge.equals(((QPPTWEdge)that).underlyingEdge);
	}
	
	@Override
	public int hashCode() {
		return this.underlyingEdge.hashCode();
	}
	
	public void addAircraftReservation(Reservation reservation) {
		this.reservations.add(reservation);
	}
	
	/**@return last reservation ending before the specified one in time, or null if none exists*/
	public Reservation getReservationBefore(long time) {
		long earliest = Long.MAX_VALUE;
		Reservation rval = null;
		for (Reservation r : reservations) {
			if ((r.endTime < time) && (r.endTime > earliest)) {
				earliest = r.endTime;
				rval = r;
			}
		}
		
		return rval;
	}
	
	/**This is just for finding the causing aircraft for delays*/
	public static class Reservation {
		private Truck truck;
		private long startTime;
		private long endTime;
		public Reservation(Truck truck, long startTime, long endTime) {
			this.truck = truck;
			this.startTime = startTime;
			this.endTime = endTime;
		}
		
		public Truck gettruck() {
			return truck;
		}
		
		public long getStartTime() {
			return startTime;
		}
		
		public long getEndTime() {
			return endTime;
		}
	}
	
	@Override
	public String toString() {
		return "Edge[" + this.getUnderlyingEdge() + ":" + this.getNodeFrom() + "-" + this.getNodeTo() + "]";
	}
}
