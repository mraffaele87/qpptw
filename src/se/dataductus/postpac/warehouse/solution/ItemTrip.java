package se.dataductus.postpac.warehouse.solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.dataductus.postpac.warehouse.domain.Truck;

/**an allocation of items to a truck to be collected in a single trip, not in any particular ordering (but grouped by the order they are for)*/
public class ItemTrip {
	/**items to get, grouped by order ID*/
	private final List<ItemsToGetForOrder> itemsToGetGroupedByOrder;
	private final List<ItemsToGetFromLocation> itemsToGetGroupedByLocation;
	private final List<ItemToGet> itemsToGetUnGrouped;
	private final Truck truck;
	private final int quantity;
	
	public ItemTrip(List<ItemToGet> itemsToGet, Truck truck) {
		this.itemsToGetUnGrouped = Collections.unmodifiableList(new ArrayList<>(itemsToGet));
		
		// group items by orderID
		List<ItemsToGetForOrder> l = new ArrayList<>();
		for (List<ItemToGet> l3 : ItemToGet.groupByOrderID(itemsToGetUnGrouped).values()) {
			l.add(new ItemsToGetForOrder(l3));
		}
		this.itemsToGetGroupedByOrder = Collections.unmodifiableList(l);
		
		// group items by location
		List<ItemsToGetFromLocation> l2 = new ArrayList<>();
		for (List<ItemToGet> l3 : ItemToGet.groupByLocation(itemsToGetUnGrouped).values()) {
			l2.add(new ItemsToGetFromLocation(l3));
		}
		this.itemsToGetGroupedByLocation = Collections.unmodifiableList(l2);
		
		this.truck = truck;
		
		int qty = 0;
		for (ItemToGet itg : this.itemsToGetUnGrouped) {
			qty += itg.getQuantity();
		}
		this.quantity = qty;
		
		if (qty > truck.getCapacity()) {
			throw new Error("WARNING: allocated " + qty + " items to truck " + truck.getId() + " which has capacity " + truck.getCapacity());
		}
	}
	
	public List<ItemsToGetForOrder> getItemsToGetGroupedByOrder() {
		return itemsToGetGroupedByOrder;
	}
	
	/**@return all items to get in this trip, processed to combine items at the same location*/
	public List<ItemToGet> getItemsToGetUnGrouped() {
		return itemsToGetUnGrouped;
	}
	
	public List<ItemsToGetFromLocation> getItemsToGetGroupedByLocation() {
		return itemsToGetGroupedByLocation;
	}
	
	public Truck getTruck() {
		return truck;
	}
	
	public int getQuantity() {
		return this.quantity;
	}
	
	public int getRemainingTruckCapacity() {
		return this.truck.getCapacity() - this.getQuantity();
	}
	
	public String toShortString() {
		StringBuffer buf = new StringBuffer();
		buf.append("ItemTrip[items=");
		boolean first = true;
		for (ItemsToGetForOrder itgs : this.itemsToGetGroupedByOrder) {
			for (ItemToGet itg : itgs.getItemsToGet()) {
				buf.append(first?"":",");
				buf.append("(id=");
				buf.append(itg.getItem().getId());
				buf.append(" locNd=");
				buf.append(itg.getLocation().getLocation().getId());
				buf.append(" locLvl=");
				buf.append(itg.getLocation().getStorageLevel());
				buf.append(" qty=");
				buf.append(itg.getQuantity());
				buf.append(" orderID=");
				buf.append(itgs.getOrderID());
				buf.append(")");
				first = false;
			}
		}
		buf.append("],truck=");
		buf.append(this.truck.getId());
		buf.append(",total=");
		buf.append(getQuantity());
		buf.append(",rem=");
		buf.append(getRemainingTruckCapacity());
		
		return buf.toString();
	}
	
	@Override
	public String toString() {
		return "ItemTrip[items=" + itemsToGetGroupedByOrder + ", truck=" + truck.getId() + ", qty=" + getQuantity() + ", remTruckCap=" + getRemainingTruckCapacity() + " ]";
	}
}