package se.dataductus.postpac.warehouse.domain.loading;

public class Resource {
	
	private final int id;
	private int node;
	private int item;
	private int quantity;
	private int level;
	
	public Resource(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Resource [id=" + id + ", nodeId=" + node + ", itemId=" + item
				+ ", quantity=" + quantity + ", level=" + level + "]";
	}
	public int getId() {
		return id;
	}
	public int getNode() {
		return node;
	}
	public void setNode(int node) {
		this.node = node;
	}
	public int getItemId() {
		return item;
	}
	public void setItem(int item) {
		this.item = item;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
}
