package se.dataductus.postpac.warehouse.domain;

public class Edge {
	
	private final int id;
	private final Node from;
	private final Node to;
	private final double length;
	private final double timeCost;
	
	public Edge(int id,Node n1,Node n2,double l,double tc){
		this.id = id;
		this.from = n1;
		this.to = n2;
		this.length = l;
		this.timeCost = tc;
	}
	
	public int getId() {
		return id;
	}
	
	public Node getFrom() {
		return from;
	}

	public Node getTo() {
		return to;
	}

	public double getLength() {
		return length;
	}

	public double getTimeCost() {
		return timeCost;
	}
	
	@Override
	public boolean equals(Object that) {
		return (that instanceof Edge) && (this.from.equals(((Edge)that).from)) && (this.to.equals(((Edge)that).to)) && (this.length == ((Edge)that).length);
	}
	
	@Override
	public int hashCode() {
		return this.from.hashCode() + this.toString().hashCode() + new Double(this.length).hashCode();
	}

	@Override
	public String toString() {
		return "Edge [fromNode=" + from.getId() + ", toNode=" + to.getId() + ", length=" + length
				+ ", timeCost=" + timeCost + "]";
	}

}
