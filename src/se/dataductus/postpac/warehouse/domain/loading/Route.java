package se.dataductus.postpac.warehouse.domain.loading;

import java.util.List;

public class Route {
	private List<NodeVisit> nodeVisits;
	
	public Route(List<NodeVisit> nodeVisits) {
		this.nodeVisits = nodeVisits;
	}
	
	public List<NodeVisit> getNodeVisits() {
		return nodeVisits;
	}
}
