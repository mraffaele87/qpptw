package se.dataductus.postpac.warehouse.domain;


/**wraps up an Item and the quantity of it required by this Order*/
public class OrderItem {
	private Item item;
	private int quantity;
	
	public OrderItem(Item item, int quantity) {
		this.item = item;
		this.quantity = quantity;
	}
	
	public Item getItem() {
		return item;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public String toString() {
		return "OrderItem[item=" + this.item.toShortString() + ", qty=" + this.quantity + "]";
	}
}