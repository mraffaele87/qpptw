package se.dataductus.postpac.warehouse.domain.loading;

public class Node {

	private int id;
	private double x;
	private double y;
	private boolean base;

	public Node(int id) {
		this(id, 0, 0);
	}
	
	public Node(int id, double x, double y){
		this(id, x, y, false);
	}
	
	public Node(int id, double x, double y, boolean base){
		this.id = id;
		this.x = x;
		this.y = y;
		this.base = base;
	}
	
	public int getId() {
		return id;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public boolean isBase() {
		return base;
	}
	
	@Override
	public String toString() {
		return "Node [id=" + id + ", x=" + x + ", y=" + y + ", base=" + base + "]";
	}
}
