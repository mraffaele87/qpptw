package se.dataductus.postpac.warehouse.domain.loading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**A set of items from one order, for a truck to get in a single trip*/
public class ItemTrip {
	private final List<ItemToGet> itemsToGet;
	private final int truckID;
	
	public ItemTrip(List<ItemToGet> itemsToGet, int truckID) {
		this.itemsToGet = new ArrayList<>(itemsToGet);
		this.truckID = truckID;
	}
	
	public List<ItemToGet> getItemsToGet() {
		return Collections.unmodifiableList(itemsToGet);
	}
	
	public int getTruckID() {
		return truckID;
	}
	
	@Override
	public String toString() {
		return "ItemAllocation[itemsToGet=" + itemsToGet + ", truckID=" + truckID + " ]";
	}
}
