package se.dataductus.postpac.warehouse.domain.loading;

public class Edge {

	private int id;
	private int from;
	private int to;
	private double length;

	public int getId() {
		return id;
	}
	
	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public Edge(int id,int a,int b,double l){
		this.id = id;
		from=a;
		to = b;
		length = l;
	}
	
	@Override
	public String toString() {
		return "Edge [id=" + id + ", from=" + from + ", to=" + to + ", length=" + length + "]";
	}
}
