package se.dataductus.postpac.warehouse.solution.qpptw;


import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.jgrapht.util.FibonacciHeap;
import org.jgrapht.util.FibonacciHeapNode;

import se.dataductus.postpac.warehouse.domain.Truck;
import se.dataductus.postpac.warehouse.solution.ItemToGet;
import se.dataductus.postpac.warehouse.solution.ItemTrip;
import se.dataductus.postpac.warehouse.solution.ItemsToGetFromLocation;

public class QPPTW {
	private boolean useTailSet;
	private int countLoop; 
	
	private final GraphWithTimeWindows twGraph;
	
	private final TimeEstimator timeEstimator;
	
	/**expressed as a multiple of the estimated travel time for an edge*/
	@SuppressWarnings("unused")
	private final double bufferTimeAfterProportionate;
	
	/**in milliseconds*/
	private final long bufferTimeAfterFixed;
	
	public QPPTW(GraphWithTimeWindows twGraph, TimeEstimator timeEstimator, boolean useTailSet) {
		this.twGraph = twGraph;
		this.timeEstimator = timeEstimator;
		this.bufferTimeAfterFixed = 0;
		this.bufferTimeAfterProportionate = 0;
		this.useTailSet = useTailSet;
		this.countLoop = 0;
	}
	
	public Route allocateRoute(double startTime, ItemTrip itemTrip) {
		QPPTWNode startNode = this.twGraph.getLoadingNode();
		QPPTWNode endNode = this.twGraph.getLoadingNode();
		
		// create a list of NodeLoading objects for the middle parts 
		List<NodeLoadingPeriod> nodesToLoadFrom = new ArrayList<>();
		Set<QPPTWNode> nodesVisited = new HashSet<>();
		for (int index = 0; index < itemTrip.getItemsToGetGroupedByLocation().size(); index++) {
			ItemsToGetFromLocation itemsToGetFromLocation = itemTrip.getItemsToGetGroupedByLocation().get(index);
			QPPTWNode node = this.twGraph.getQPPTWNode(itemsToGetFromLocation.getLocation().getLocation());
			if (!nodesVisited.contains(node)) {
				double duration = itemsToGetFromLocation.getTimeCostForLoading();
				List<ItemToGet> itemsFromThisNode = new ArrayList<>();
				itemsFromThisNode.addAll(itemsToGetFromLocation.getItemsToGet());
				
				// check for other items at this node
				for (int j = index + 1; j < itemTrip.getItemsToGetGroupedByLocation().size(); j++) {
					ItemsToGetFromLocation itemsToGetFromLocation2 = itemTrip.getItemsToGetGroupedByLocation().get(j);
					if (itemsToGetFromLocation2.getLocation().getLocation().equals(node.getUnderlyingNode())) {
						duration += itemsToGetFromLocation2.getTimeCostForLoading();
						itemsFromThisNode.addAll(itemsToGetFromLocation2.getItemsToGet());
					}
				}
				
				nodesToLoadFrom.add(new NodeLoadingPeriod(node, duration, itemsFromThisNode));
				nodesVisited.add(node);
			}
		}
		
		Route rval = allocateSubRoute(startNode, endNode, nodesToLoadFrom, startTime, itemTrip.getTruck());
		
		if (rval == null) {
			String s = Integer.toString(startNode.getUnderlyingNode().getId());
			for (NodeLoadingPeriod nlp : nodesToLoadFrom) {
				s += "->" + nlp.getNode().getUnderlyingNode().getId();
			}
			s += "->" + endNode.getUnderlyingNode().getId();
			
			System.out.println("WARNING : Couldn't find route covering nodes " + s + " starting at " + startTime  + " for " + itemTrip.getTruck());
		}
		
		rval.setItemTrip(itemTrip);
		
		readjustTimeWindows(rval, itemTrip.getTruck());
		
		return rval;
	}
	
	/**
	 * The core QPPTW algorithm. Allocate a conflict-free route to the specified truck.
	 * (algorithm 1 in Ravizza2013) 
	 * modified to have multiple intermediate nodes for loading from
	 * 
	 * does not alter the timewindows of edges
	 */
	public Route allocateSubRoute(QPPTWNode startNode, QPPTWNode endNode, List<NodeLoadingPeriod> nodesToLoadFrom, double startTime, Truck truck) {
		// init
		QPPTWNode s_i = startNode; // source node
		//NodeLoadingPeriod targetNodeLoadingPeriod = nodesToLoadFrom.remove(0);
		//QPPTWNode t_i = targetNodeLoadingPeriod.getNode(); // terminal node - this updates as we go
		double time_i = startTime; // initial time
		
		// line 1
		FibonacciHeap<Label> H = new FibonacciHeap<Label>(); // JGraphT FibonacciHeap - elements are wrapped in FibonacciHeapNode objects, which contain a key (heap is sorted according to the key values) 
		
		// line 2 - might be more efficient to store these in the Node objects
		Map<QPPTWNode,Set<Label>> Lvs = new HashMap<QPPTWNode,Set<Label>>();
		for (QPPTWNode n : this.twGraph.getNodes().values()) {
			Set<Label> s = new HashSet<Label>();
			Lvs.put(n, s);
		}
		
		// line 3
		double cost = time_i;
		Label L = new Label(s_i, (nodesToLoadFrom.isEmpty() ? endNode : nodesToLoadFrom.get(0).getNode()), 0, new TimePeriod(time_i, Long.MAX_VALUE), null, cost, truck);

		// line 4
		L.associatedNode = new FibonacciHeapNode<QPPTW.Label>(L);
		H.insert(L.associatedNode, L.getCost());
		
		// line 5 - add label to source or terminal node as appropriate
		Lvs.get(s_i).add(L);
		
		// line 6
		while (!H.isEmpty()) {
			// line 7 - take the minimal element from the heap (that is, the label with the earliest possible start time)
			L = H.removeMin().getData();
			L.associatedNode = null; // this shows that L is no longer in the heap so we won't try to delete it at line 29
			
			// line 8
			if (L.vL == L.getSubrouteEnd()) { // this label represents a route to the target
				if (L.vL.equals(endNode)) { // this is actually the end of the route
					// line 9
					Route R = L.constructRoute(truck);
					
					// line 10
					return R;
				} else {
					// okay, this is a loading node. this label is the time of arrival at the node
					
					// the time of exit from the edge before the loading node is checked
					// against the windows allowing for the extra time needed to load.
					// however, the label added is just for the travel along the edge to reach the node
					// we need to add another label which covers the node loading period, to be 
					// used as the starting label for the next stage of the route;
					// earliest time of arrival is 
					// same as previous, but with loading time added
					// but still need to do the dominance checks etc
					
					// if we were at the end of a subroute, add an extra label to represent the loading period
					// with an updated target node
					// subroute end is either the next loading node, or if none of those left, the end of the route
					int subrouteEndIndex = L.getSubrouteEndIndex() + 1;
					QPPTWNode subrouteEnd = (subrouteEndIndex < nodesToLoadFrom.size()) ? nodesToLoadFrom.get(subrouteEndIndex).getNode() : endNode;
					double earliestTimeCanLeave = L.IL.getA() + nodesToLoadFrom.get(L.getSubrouteEndIndex()).getDuration();
					
					// time period for leaving this node runs from the earliestTimeOfArrival+loadingTime to the latestTimeOfArrival (as restricted by time windows)
					Label LLoading = new Label(L.vL, subrouteEnd, subrouteEndIndex, new TimePeriod(earliestTimeCanLeave, L.IL.getB()), L, earliestTimeCanLeave, truck);
					LLoading.setNodeLoadingPeriod(nodesToLoadFrom.get(L.getSubrouteEndIndex()));
					
					// no need to add to heap? it'll just be removed anyway
					//LLoading.associatedNode = new FibonacciHeapNode<QPPTW.Label>(LLoading);
					//H.insert(LLoading.associatedNode, LLoading.getCost());
					
					// rather, we just replace the recently obtained node with the new one
					L = LLoading;
				}
			}
			
			// implementation as in paper. changed version for "backwards" version is not used here
			// line 11 - for each outgoing edge
			for (QPPTWEdge e : this.twGraph.getGraph().outgoingEdgesOf(L.vL)) {
				
				SortedSet<TimeWindow> stw;
				if (useTailSet) {
					stw = e.getSetOfSortedTimeWindows().tailSet(new TimeWindow(L.IL.getA(), L.IL.getA()), true); // we only want time windows that end after this label starts
				} else {
					stw = e.getSetOfSortedTimeWindows();
				}
				
				// line 12 - for each time-window
				timeWindowLoop:
				for (TimeWindow Fj_eL : stw) {
					this.countLoop++;
					
					// line 13
					/*Expand labels for edges where time intervals overlap*/
					
					// line 14 - does this time window start before the label ends?
					if (Fj_eL.getStartTime() > L.IL.getB()) { // that is, window starts after this label ends
						// line 15
						break timeWindowLoop; /*if not, consider the next outgoing edge*/
					}
					
					// line 16 - does this time window end before the label starts? 
					if (Fj_eL.getEndTime() < L.IL.getA()) { 
						// line 17
						continue timeWindowLoop; /*if so, consider the next time-window*/
					}
					
					// line 18 - time of entry to edge (either the time of arrival, or the start of the time-window, whichever is latest)
					double time_in = Math.max(L.IL.getA(), Fj_eL.getStartTime());
					
					// line 19
					/*a^j_eL > a_L => waiting*/
					double waitTime = 0;
					if (Fj_eL.getStartTime() > L.IL.getA()) {
						waitTime = Fj_eL.getStartTime() - L.IL.getA();
					}
					
					// line 20 - earliest possible time that edge will be exited
					double time_out = time_in + this.timeEstimator.estimateTime(e);
					
					// if this is an edge leading to one of the loading nodes, add on the loading time so any windows that are found include loading too 
					double time_out_forWindowCheck = time_out;
					boolean isAtLoadingNode = (L.getSubrouteEndIndex() < nodesToLoadFrom.size()) && e.getNodeTo().equals(L.getSubrouteEnd());
					if (isAtLoadingNode) {
						time_out_forWindowCheck += nodesToLoadFrom.get(L.getSubrouteEndIndex()).getDuration();
					}
					
					// line 21 - if the exit time is within the time-window (allowing for buffer time), then expand the route
					if ((time_out_forWindowCheck + bufferTimeAfter(startNode, L.vL, e)) <= Fj_eL.getEndTime()) {
						// line 22 - get the node that the current edge leads to.
						QPPTWNode u = (e.getNodeFrom() != L.vL) ? e.getNodeFrom() : e.getNodeTo();
						
						// line 23 - make a new label
						// covers time period of time_out (earliest arrival time at node) to Fj_eL.getEndTime() (latest arrival time, the end of the window)
						Label LDash = new Label(u, L.getSubrouteEnd(), L.getSubrouteEndIndex(), new TimePeriod(time_out, Fj_eL.getEndTime()), L, time_out, truck);
						LDash.setWaitTime(waitTime);
						
						// line 24
						/*dominance check*/
						
						// line 25
						Set<Label> Lu = Lvs.get(u);
						List<Label> toRemove = new LinkedList<>();
						for (Label lHat : Lu) {
							
							// SB only perform dominance check on labels for the same subroute
							if (LDash.getSubrouteEnd().equals(lHat.getSubrouteEnd())) {
								// line 26
								if (lHat.dominates(LDash)) {
									// line 27
									continue timeWindowLoop; /*next time window*/
								}
								
								// line 28
								if (LDash.dominates(lHat)) {
									// line 29
									if (lHat.associatedNode != null) { // SB - if not already removed from heap, remove now
										H.delete(lHat.associatedNode);
										lHat.associatedNode = null;
									}
									
									// line 30
									toRemove.add(lHat);
								}
							}
						} // end of line 25 foreach
						Lu.removeAll(toRemove);
						
						// lines 31 and 32 add the label to the heap, and the set of labels for each node
						// line 31
						LDash.associatedNode = new FibonacciHeapNode<QPPTW.Label>(LDash);
						H.insert(LDash.associatedNode, LDash.getCost());
						
						// line 32
						Lu.add(LDash);
					} // end of line 21 if
				} // end of line 12 if
			} // end of line 11 for
		} // end of line 6 while
		
		// line 33
		return null;
	}
	
	public int getCountLoop() {
		return countLoop;
	}
	
	/**
	 * Update the time-windows in the graph according to the specified route
	 * algorithm 2 in Ravizza2013
	 * but - also processes "NodeStands" - periods where a truck stands on a node loading/unloading
	 * Truck needed so we can track which truck caused a time window to be trimmed and detect which ones are blocking others 
	 * */
	@SuppressWarnings("unused")
	public void readjustTimeWindows(Route route, Truck truck) {
		// line 1
		reservationLoop:
		for (RouteStep rs : route.getSteps()) {
			Set<QPPTWEdge> conflictingEdges;
			if (rs instanceof RouteEdgeReservation) {
				conflictingEdges = ((RouteEdgeReservation)rs).getEdge().getConflictingEdges();
			} else {
				conflictingEdges = this.twGraph.getGraph().edgesOf(((RouteNodeLoading)rs).getNode());
			}
			
			// line 2
			conflictingEdgeLoop:
			for (QPPTWEdge e : conflictingEdges) { // conflicting edges set also includes the edge itself
				readjustTimeWindowsForEdge(e, rs.getTimePeriod());
			} // end of line 2 for
		} // end of line 1 for
	}
	
	private void readjustTimeWindowsForEdge(QPPTWEdge e, TimePeriod timePeriod) {
		//System.out.println(e.getSetOfSortedTimeWindows().size());
		//SortedSet<TimeWindow> s = e.getSetOfSortedTimeWindows().headSet(new TimeWindow(timePeriod.getB(),timePeriod.getB())); // get all time windows starting before the time reservation ends
		SortedSet<TimeWindow> s = e.getSetOfSortedTimeWindows();
		//System.out.println("->" + s.size());
		List<TimeWindow> toRemove = new LinkedList<>();
		List<TimeWindow> toAdd = new LinkedList<>();
		
		// line 3
		timeWindowLoop:
		for (TimeWindow Fj_e : s) {
			// line 4
			if (timePeriod.getB() <= Fj_e.getStartTime()) {
				// line 5
				//continue conflictingEdgeLoop;/*time-window is too late*/
				break timeWindowLoop;
			}
			
			// line 6
			if (timePeriod.getA() < Fj_e.getEndTime()) {
				// line 7
				/*otherwise, time-window is too early*/
				
				// line 8 - remove window if it would become too short to be useful
				if (timePeriod.getA() < Fj_e.getStartTime() + e.getMinTaxiTime()) { // the reservation starts less than the min taxi time after the start of the window
					// line 9
					if (Fj_e.getEndTime() - e.getMinTaxiTime() < timePeriod.getB()) { // the reservation ends less than the min taxi time before the end of the window
						// line 10 - delete the window
						toRemove.add(Fj_e);
					
					// line 11 - the reservation ends more than the min taxi time before the end of the window
					} else {
						// line 12
						/*shorten start time of window - TimeWindow objects are immutable, so delete the old one and add a new one*/
						
						// line 13
						TimeWindow tw = new TimeWindow(timePeriod.getB(), Fj_e.getEndTime());
						toRemove.add(Fj_e);
						toAdd.add(tw);
					}
					
				// line 14
				} else { // reservation starts more than the min taxi time after the start of the window
					// line 15 - reservation ends less than the min taxi time before the end of the window
					if (Fj_e.getEndTime() - e.getMinTaxiTime() < timePeriod.getB()) {
						// line 16
						/*shorten end of time-window*/
						
						// line 17 - delete old and create new
						TimeWindow tw = new TimeWindow(Fj_e.getStartTime(), timePeriod.getA());
						toRemove.add(Fj_e);
						toAdd.add(tw);
						
					// line 18 - reservation splits window into two chunks, both larger than the min taxi time
					} else {
						// line 19
						/*split time-window*/
						
						// line 20
						TimeWindow tw1 = new TimeWindow(Fj_e.getStartTime(), timePeriod.getA());
						
						// line 21
						TimeWindow tw2 = new TimeWindow(timePeriod.getB(), Fj_e.getEndTime());
						
						toRemove.add(Fj_e);
						toAdd.add(tw1);
						toAdd.add(tw2);
					}
				} // end of line 8 if
			} // end of line 6 if
		} // end of line 3 for
		
		e.getSetOfSortedTimeWindows().removeAll(toRemove);
		e.getSetOfSortedTimeWindows().addAll(toAdd);
	}
	
	/**how long should the buffer after an edge be?
	 * @param start and current - needed if heuristically deciding buffer time based on distance from journey start
	 * @param e - the edge if calculating time proportionate to edge length
	 * @return pad time in milliseconds
	 */
	private long bufferTimeAfter(QPPTWNode start, QPPTWNode current, QPPTWEdge e) {
		return this.bufferTimeAfterFixed;
	}
	
	private class Label {
		/**node at end of subroute this label forms a part of*/
		private QPPTWNode subrouteEnd;
		
		/**index in the list of node loading periods that represents the end of this label's subroute*/
		private int subrouteEndIndex;
		
		/**vertex reached*/
		private QPPTWNode vL;
		
		/**times that aircraft could reach this */
		private TimePeriod IL;
		
		/**previous label on the route*/
		private Label predL;
		
		/** not in the paper, but helpful to keep this separate */
		private double cost;
		
		private FibonacciHeapNode<Label> associatedNode;
		
		private Truck truck;
		
		private double waitTime;
		
		/**only applies if this label represents a period of loading at a node*/
		private NodeLoadingPeriod nodeLoadingPeriod;
		
		public Label(QPPTWNode vertex, QPPTWNode subrouteEnd, int subrouteEndIndex, TimePeriod timePeriod, Label previousLabel, double cost, Truck truck) {
			this.vL = vertex;
			this.subrouteEnd = subrouteEnd;
			this.subrouteEndIndex = subrouteEndIndex;
			this.IL = timePeriod;
			this.predL = previousLabel;
			this.cost = cost;
			this.truck = truck;
			this.waitTime = 0;
			this.nodeLoadingPeriod = null;
		}
		
		public boolean dominates(Label that) { // section 4.3 of Ravizza2013
			return (this.IL.getA() <= that.IL.getA()) && (this.IL.getB() >= that.IL.getB());
		}
		
		public double getCost() {
			return cost;
		}
		
		public QPPTWNode getSubrouteEnd() {
			return subrouteEnd;
		}
		
		public int getSubrouteEndIndex() {
			return subrouteEndIndex;
		}
		
		public void setWaitTime(double waitTime) {
			this.waitTime = waitTime;
		}
		
		@SuppressWarnings("unused")
		public double getWaitTime() {
			return waitTime;
		}
		
		@SuppressWarnings("unused")
		public Truck getTruck() {
			return truck;
		}
		
		public void setNodeLoadingPeriod(NodeLoadingPeriod nodeLoadingPeriod) {
			this.nodeLoadingPeriod = nodeLoadingPeriod;
		}
		
		public NodeLoadingPeriod getNodeLoadingPeriod() {
			return nodeLoadingPeriod;
		}
		
		// work backwards from current label to get route (or forwards for a "backwards" routing)
		public Route constructRoute(Truck t) {
			LinkedList<RouteStep> routeSteps = new LinkedList<>();
			LinkedList<Route.NodeVisit> nodeVisits = new LinkedList<>();
			Label l = this;
			nodeVisits.addFirst(new Route.NodeVisit(l.vL, l.IL));
			double edgeBeginTime = Double.POSITIVE_INFINITY;
			double edgeEndTime = Double.NEGATIVE_INFINITY;
			
			while (l.predL != null) { // work back until we reach that start, where there's no previous label
				// skip labels for loading nodes (times for these are still picked up for edgeBeginTime)
				if (!l.vL.equals(l.predL.vL)) {
					// add edge connecting current node to previous
					// - note: code to retrieve edge from graph returns an array, 
					// but just get first element as the graph currently only ever has one edge between a pair of vertices
					QPPTWEdge edge = (QPPTWEdge)(twGraph.getGraph().getAllEdges(l.predL.vL, l.vL).toArray()[0]);
					double time = timeEstimator.estimateTime(edge);
	
					if (l == Label.this) { // if this is the end of the route
						edgeEndTime = this.IL.getA(); // end time if this label's earliest arrival time
						edgeBeginTime = Math.min(edgeEndTime - time, l.predL.IL.getB()); // begin time is the earliest of (endTime-transitTime) and the latest arrival time at the start of the edge
					} else {
						edgeEndTime = edgeBeginTime; // end time of this edge is the start time of previous edge
						edgeBeginTime = Math.min(l.IL.getA() - time, l.predL.IL.getB());
					}
					
					RouteEdgeReservation e = new RouteEdgeReservation(edge, new TimePeriod(edgeBeginTime, edgeEndTime), t);
					
					// waiting? yes if traversal was longer than expected
					//double waitTime = (e.getTimePeriod().b - e.getTimePeriod().a) - taxiTimeEstimator.estimateTime(l.predL.vL, l.vL, Arrays.asList(new Edge[]{e.getEdge()})).getProbableTime();
					// as a result of later debugging, l.getWaitTime() will produce the same value
					double waitTime = (edgeEndTime - edgeBeginTime) - time;
					if (waitTime > 0) {
						e.setWaitTime(waitTime);
					}
					
					routeSteps.addFirst(e);
					
					// add to node visits
					Route.NodeVisit nv = new Route.NodeVisit(l.predL.vL, l.predL.IL);
					
					nodeVisits.addFirst(nv);
				} else {
					edgeEndTime = l.IL.getA();
					edgeBeginTime = l.predL.IL.getA(); // if it was a loading period, update edgeBeginTime to the start of the loading period
					RouteNodeLoading nl = new RouteNodeLoading(l.vL, new TimePeriod(edgeBeginTime, edgeEndTime), l.getNodeLoadingPeriod().getItems(), t);
					
					//System.out.println("Loading: " + edgeBeginTime + "-" + edgeEndTime + ", " + l.getNodeLoadingPeriod().getItems());
					
					routeSteps.addFirst(nl);
				}
				
				// set current node to previous
				l = l.predL;
			}
			Route r = new Route(routeSteps, nodeVisits);
			return r;
		}
	}
	
	/**represents a requirement to sit on a node loading*/
	private class NodeLoadingPeriod {
		private final QPPTWNode node;
		private final double duration;
		private final List<ItemToGet> items;

		public NodeLoadingPeriod(QPPTWNode node, double duration, List<ItemToGet> items) {
			this.node = node;
			this.duration = duration;
			this.items = items;
		}
		
		public QPPTWNode getNode() {
			return node;
		}
		
		public double getDuration() {
			return duration;
		}
		
		public List<ItemToGet> getItems() {
			return items;
		}
	}
}
