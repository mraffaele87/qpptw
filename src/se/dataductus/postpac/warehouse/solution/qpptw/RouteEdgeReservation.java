package se.dataductus.postpac.warehouse.solution.qpptw;

import se.dataductus.postpac.warehouse.domain.Truck;

public class RouteEdgeReservation extends RouteStep {
	private QPPTWEdge edge;
	
	/** for analysis rather than part of the algorithm (at least for now) */
	private double waitTime;

	/**
	 * @param edge - the edge the reservation applies to
	 * @param timePeriod - the earliest and latest times that the aircraft may be on this edge
	 * @param truck - the truck that the edge is reserved for
	 */
	public RouteEdgeReservation(QPPTWEdge edge, TimePeriod timePeriod, Truck truck) {
		super(timePeriod, truck);
		this.edge = edge;
		this.waitTime = 0;
	}
	
	public QPPTWEdge getEdge() {
		return edge;
	}
	
	public void setWaitTime(double waitTime) {
		this.waitTime = waitTime;
	}
	
	public double getWaitTime() {
		return waitTime;
	}
	
	/**
	 * the time period is the time of entry to the edge to the time of exit
	 * this might be longer than the minimum traversal time, if there needs to be a wait
	 */
	public TimePeriod getTimePeriod() {
		return super.getTimePeriod();
	}
	
	@Override
	public String toString() {
		return "ER[" + this.edge.getNodeFrom().getUnderlyingNode().getId() + ">" + this.edge.getNodeTo().getUnderlyingNode().getId() + "|" + this.getTimePeriod().getA() + "-" + this.getTimePeriod().getB() + "]";
	}
}
