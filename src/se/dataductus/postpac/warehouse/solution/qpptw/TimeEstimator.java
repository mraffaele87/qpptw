package se.dataductus.postpac.warehouse.solution.qpptw;

import java.util.List;

public class TimeEstimator {
	private double speed;
	private long callCount;
	
	public TimeEstimator(double speed) {
		this.speed = speed;
		this.callCount = 0;
	}
	
	/**returns time in milliseconds to travel from start to end (edges is only required by some methods)*/
	public double estimateTime(List<QPPTWEdge> edges) {
		this.callCount++;

		double time = 0;
		for (QPPTWEdge e : edges) {
			time += (e.getLength() / speed);
		}
		
		return time;
	}
	
	public double estimateTime(QPPTWEdge edge) {
		this.callCount++;

		double time = edge.getLength() / speed;
		
		return time;
	}
	
	public long getCallCount() {
		return callCount;
	}
}
