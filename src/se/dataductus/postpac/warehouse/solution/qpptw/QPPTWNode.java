package se.dataductus.postpac.warehouse.solution.qpptw;

public class QPPTWNode {
	private final double xCoordinate;
	private final double yCoordinate;
	private final se.dataductus.postpac.warehouse.domain.Node underlyingNode;
	
	public QPPTWNode(se.dataductus.postpac.warehouse.domain.Node underlyingNode) {
		this.xCoordinate = underlyingNode.getX();
		this.yCoordinate = underlyingNode.getY();
		this.underlyingNode = underlyingNode;
	}
	
	public double getxCoordinate() {
		return xCoordinate;
	}

	public double getyCoordinate() {
		return yCoordinate;
	}
	
	public se.dataductus.postpac.warehouse.domain.Node getUnderlyingNode() {
		return underlyingNode;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("QPPTWNode[");
		//buf.append(this.xCoordinate);
		//buf.append(",");
		//buf.append(this.yCoordinate);
		//buf.append(",");
		buf.append(this.underlyingNode);
		buf.append("]");
		
		return buf.toString();
	}
}
