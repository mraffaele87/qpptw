package se.dataductus.postpac.warehouse.solution.qpptw;

import java.util.Comparator;

/**
 * immutable representation of a time-window
 * These represent reservations on edges; currently contain same data as the generic TimePeriod class
 * but implemented separately as it does something a bit different
 */
public final class TimeWindow {
	/**earliest time that can be allocated (inclusive)*/
	private final double startTime;
	
	/**latest time that can be allocated (inclusive)*/
	private final double endTime;
	
	public TimeWindow (double start, double end) {
		this.startTime = start;
		this.endTime = end;
	}
	
	public double getStartTime() {
		return startTime;
	}
	
	public double getEndTime() {
		return endTime;
	}
	
	/**compares based on startTimes*/
	public static class StartTimeComparator implements Comparator<TimeWindow> {
		@Override
		public int compare(TimeWindow a, TimeWindow b) {
			return Double.compare(a.startTime, b.startTime);
		}
	}
	
	/**
	 * compares based on start and end times:
	 * if the compared TW is wholly before this TW, then its "less than"
	 * if the compared TW is wholly after this TW, then its "greater than" 
	 * if the compared TW overlaps this TW, then its "equal to"
	 */
	public static class StartAndEndTimeComparator implements Comparator<TimeWindow> {
		@Override
		public int compare(TimeWindow a, TimeWindow b) {
			if (a.endTime < b.startTime) { // a ends before b, so a < b 
				return -1;
			} else if (a.startTime > b.endTime) { // a starts after b, so a > b
				return 1;
			} else { // a overlaps b, so a == b
				return 0;
			}
		}
	}
	
	@Override
	public String toString() {
		return "TimeWindow[" + this.startTime + "-" + this.endTime + "]";
	}
}