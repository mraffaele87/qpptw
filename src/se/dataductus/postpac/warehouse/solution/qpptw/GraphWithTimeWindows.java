package se.dataductus.postpac.warehouse.solution.qpptw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DirectedWeightedMultigraph;

import se.dataductus.postpac.warehouse.Graph;
import se.dataductus.postpac.warehouse.domain.Truck;

/**
 * Representation of the graph, but for a particular solution of the problem so we have QPPTW edges and nodes
 * with corresponding timewindows
 */
public class GraphWithTimeWindows {
	private DirectedWeightedMultigraph<QPPTWNode,QPPTWEdge> graph;
	
	/**all nodes in the graph, keyed by static node ID*/
	private Map<Integer,QPPTWNode> nodes;
	
	/**all edges in the graph, keyed by static edge*/
	private Map<se.dataductus.postpac.warehouse.domain.Edge,QPPTWEdge> edges;
	
	private Graph staticGraph;
	
	/**
	 * @param staticGraph - the static graph of nodes/edges
	 */
	public GraphWithTimeWindows(Graph staticGraph, TimeEstimator te, double timeWindowsStart, double timeWindowsEnd) {
		this.staticGraph = staticGraph;
		this.graph = new DirectedWeightedMultigraph<QPPTWNode, QPPTWEdge>(QPPTWEdge.class);
		this.nodes = new HashMap<>();
		this.edges = new HashMap<>();
		
		for (se.dataductus.postpac.warehouse.domain.Node staticNode : staticGraph.getNodes()) {
			QPPTWNode node = new QPPTWNode(staticNode);
			this.nodes.put(staticNode.getId(), node);
			this.graph.addVertex(node);
		}
		
		for (se.dataductus.postpac.warehouse.domain.Edge staticEdge : staticGraph.getEdges()) {
			QPPTWEdge edge = new QPPTWEdge(staticEdge, this.nodes.get(staticEdge.getFrom().getId()), this.nodes.get(staticEdge.getTo().getId()));
			this.edges.put(staticEdge, edge);
			this.graph.addEdge(edge.getNodeFrom(), edge.getNodeTo(), edge);
		}
		
		this.calcEdgeMinimumTaxiTimes(te);
		this.initialiseTimeWindows(timeWindowsStart, timeWindowsEnd);
		for (QPPTWEdge e : this.edges.values()) {
			e.determineConflictingEdges(this.edges.values(), staticGraph.getMinimumSeparation());
		}
	}
	
	public QPPTWNode getLoadingNode() {
		return getQPPTWNode(this.staticGraph.getLoadingNode());
	}
	
	public QPPTWNode getQPPTWNode(se.dataductus.postpac.warehouse.domain.Node node) {
		return this.nodes.get(node.getId());
	}
	
	private void calcEdgeMinimumTaxiTimes(TimeEstimator te) {
		for (QPPTWEdge e : this.edges.values()) {
			e.setMinTaxiTime(te.estimateTime(e));
		}
	}
	
	/**
	 * reset time windows for all edges to the specified period
	 */
	public void initialiseTimeWindows(double startTime, double endTime) {
		for (QPPTWEdge e : this.edges.values()) {
			e.setInitialTimePeriod(startTime, endTime);
		}
	}
	
	/**is a move of a specified truck from edge 1 to edge 2 allowed (maybe there's too tight an angle? Or something else?)
	 * currently unimplemented - always returns true*/
	public boolean isMoveLegal(QPPTWEdge from, QPPTWEdge to, Truck t) {
		return true; // currently no logic implemented here
	}
	
	public List<QPPTWEdge> getDijkstraShortestPathBetweenNodes(QPPTWNode from, QPPTWNode to) {
		DijkstraShortestPath<QPPTWNode, QPPTWEdge> dsp = new DijkstraShortestPath<QPPTWNode, QPPTWEdge>(this.graph, from, to);
		return dsp.getPathEdgeList();
	}
	
	public Map<Integer, QPPTWNode> getNodes() {
		return nodes;
	}
	
	public DirectedWeightedMultigraph<QPPTWNode, QPPTWEdge> getGraph() {
		return graph;
	}
}
