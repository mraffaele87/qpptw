package se.dataductus.postpac.warehouse.solution.qpptw;

import se.dataductus.postpac.warehouse.domain.Truck;

/**a point in a route - either an edge being traversed, or a node being loaded at*/
public class RouteStep {
	private final TimePeriod timePeriod;
	private final Truck truck;
	
	public RouteStep(TimePeriod timePeriod, Truck truck) {
		this.timePeriod = timePeriod;
		this.truck = truck;
	}
	
	public TimePeriod getTimePeriod() {
		return timePeriod;
	}
	
	public Truck getTruck() {
		return truck;
	}
}
