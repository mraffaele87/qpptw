package se.dataductus.postpac.warehouse.domain.loading;

import java.util.List;


/**descriptor of a truck*/
public class Truck {
	private final int id;
	private final int capacity;
	private final List<Route> routes;
	
	public Truck(int id, int capacity) {
		this(id, capacity, null);
	}
	
	public Truck(int id, int capacity, List<Route> routes) {
		this.id = id;
		this.capacity = capacity;
		this.routes = routes;
	}

	public int getId() {
		return id;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public List<Route> getRoutes() {
		return routes;
	}
	
	@Override
	public String toString() {
		return "Truck [id=" + id + ", capacity=" + capacity + "]";
	}
}
