package se.dataductus.postpac.warehouse.domain.loading;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/** 
 *  Class for writing/reading JSON corresponding to problem data
 *  Static data like graph, item IDs / names, costs always present
 *  Can also include data related to specific problem instances:
 *  item locations and orders for sub problem A
 *  item locations and allocations to trucks for sub problem B
 */
public class Problem {

	private double[] levelCosts;
	private double mixedItemFixedCost;
	private double truckSpeed;
	private double minimiumSeparation;
	private int itemIdRange;
	private Map<Integer,String> itemNames;
	private List<Resource> resources;
	private List<Node> nodes;
	private List<Edge> edges;
	private List<Truck> trucks;
	private List<Order> orders;
	private List<ItemTrip> itemTrips;
	
	/**identifies the base - where trucks take items to*/
	private int loadingNodeID;
	
	/**the time cost associated with loading per item*/
	private double timeCostForLoadingOneItem;
	
	/**the time cost associated with unloading per item*/
	private double timeCostForUnloadingOneItem;
	
	public double[] getLevelCosts() {
		return levelCosts;
	}
	public void setLevelCosts(double[] levelCosts) {
		this.levelCosts = levelCosts;
	}
	public double getMixedItemFixedCost() {
		return mixedItemFixedCost;
	}
	public void setMixedItemFixedCost(double mixedItemFixedCost) {
		this.mixedItemFixedCost = mixedItemFixedCost;
	}
	public double getTruckSpeed() {
		return truckSpeed;
	}
	public void setTruckSpeed(double truckSpeed) {
		this.truckSpeed = truckSpeed;
	}
	public double getMinimiumSeparation() {
		return minimiumSeparation;
	}
	public void setMinimiumSeparation(double minimiumSeparation) {
		this.minimiumSeparation = minimiumSeparation;
	}
	public int getItemIdRange() {
		return itemIdRange;
	}
	public void setItemIdRange(int itemIdRange) {
		this.itemIdRange = itemIdRange;
	}
	public Map<Integer, String> getItemNames() {
		return itemNames;
	}
	public void setItemNames(Map<Integer, String> itemNames) {
		this.itemNames = itemNames;
	}
	public List<Resource> getResources() {
		return resources;
	}
	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}
	public List<Edge> getEdges() {
		return edges;
	}
	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}
	public List<Node> getNodes() {
		return nodes;
	}
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
	public List<Truck> getTrucks() {
		return trucks;
	}
	public void setTrucks(List<Truck> trucks) {
		this.trucks = trucks;
	}
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public List<ItemTrip> getItemTrips() {
		return itemTrips;
	}
	public void setItemTrips(List<ItemTrip> itemTrips) {
		this.itemTrips = itemTrips;
	}
	public int getLoadingNodeID() {
		return loadingNodeID;
	}
	public void setLoadingNodeID(int loadingNodeID) {
		this.loadingNodeID = loadingNodeID;
	}
	public double getTimeCostForLoadingOneItem() {
		return timeCostForLoadingOneItem;
	}
	public void setTimeCostForLoadingOneItem(double timeCostForLoadingOneItem) {
		this.timeCostForLoadingOneItem = timeCostForLoadingOneItem;
	}
	public double getTimeCostForUnloadingOneItem() {
		return timeCostForUnloadingOneItem;
	}
	public void setTimeCostForUnloadingOneItem(
			double timeCostForUnloadingOneItem) {
		this.timeCostForUnloadingOneItem = timeCostForUnloadingOneItem;
	}
	
	public void averageItemsPerTrip(){
		int totalItems = 0;
		for(ItemTrip it : itemTrips){
			//System.out.println(it.getTruckID()+"   "+it.getItemsToGet());
			for(ItemToGet itg : it.getItemsToGet()){
				totalItems += itg.getQuantity();
			}
		}
		
		//System.out.println("AVERAGE"+ ((double)totalItems)/((double)itemTrips.size()));
	}
	
	@Override
	public String toString() {
		return "Problem [levelCosts=" + Arrays.toString(levelCosts)
				+ ", mixedItemFixedCost=" + mixedItemFixedCost
				+ ", truckSpeed=" + truckSpeed + ", itemIdRange=" + itemIdRange
				+ ", itemNames=" + itemNames + ", resources=" + resources
				+ ", edges=" + edges + "]";
	}
	
	
	public static Problem loadFromFile(File f) throws FileNotFoundException {
		return new Gson().fromJson(new InputStreamReader(new FileInputStream(f)),Problem.class);
	}
	
	public static Problem loadFromFile(File base,File resources,File jobs) throws FileNotFoundException {
		Problem a = new Gson().fromJson(new InputStreamReader(new FileInputStream(base)),Problem.class);
		Problem b = new Gson().fromJson(new InputStreamReader(new FileInputStream(resources)),Problem.class);
		Problem c = new Gson().fromJson(new InputStreamReader(new FileInputStream(jobs)),Problem.class);
		a.setResources(b.getResources());
		a.setOrders(c.getOrders());
		return a;
	}
	
	public static Problem loadFromFile(String s) throws FileNotFoundException {
		// return new Gson().fromJson(new InputStreamReader(Problem.class.getResourceAsStream(s)),Problem.class);
		// return new Gson().fromJson(new InputStreamReader(new FileInputStres),Problem.class);
		return loadFromFile( new File( s ) );
	}
	
	public void writeToFile(File f) throws FileNotFoundException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String s = gson.toJson(this);
		PrintWriter out = new PrintWriter(new FileOutputStream(f));
		out.println(s);
		out.close();
	}
	
	public void writeToFile(String s) throws FileNotFoundException {
		writeToFile(new File(s));
	}
}
