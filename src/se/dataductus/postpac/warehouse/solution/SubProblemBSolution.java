package se.dataductus.postpac.warehouse.solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import se.dataductus.postpac.warehouse.Graph;
import se.dataductus.postpac.warehouse.domain.Truck;
import se.dataductus.postpac.warehouse.solution.qpptw.GraphWithTimeWindows;
import se.dataductus.postpac.warehouse.solution.qpptw.QPPTW;
import se.dataductus.postpac.warehouse.solution.qpptw.Route;
import se.dataductus.postpac.warehouse.solution.qpptw.TimeEstimator;

/**
 * wraps up a solution for sub-problem B: that is, a permutation of tasks/trips (the order in which they'll be routed), 
 * and a permutation of items within each trip
 * in practice this is just one big permutation, the order in which 
 * fitness is total collection time, estimated by running QPPTW on the set of jobs
 */
public class SubProblemBSolution {
	private boolean useTailSet;
	private int countLoops;
	
	private final Random random;
	
	/**the static data connected with the problem*/
	private final Graph graph;
	
	/**the allocations of jobs to trucks, grouped by order*/
	private final SubProblemASolution subProblemASolution;
	
	/**the permutation of trips represented by this solution*/
	private List<ItemTrip> tripsToRoute;
	
	/**the routes, grouped by truck, allocated for this solution*/
	private Map<Truck, List<Route>> routes;
	
	/**a primitive double for the moment*/
	private double fitness;
	
	// weights for fitness (currently only one?)
	private static double fitnessWeightTotalTime = 1.0;
	
	// weights for mutation
	private static double mutationProbabilitySwapItemsInTrip = 0.5;
	private static double mutationProbabilitySwapTrips = 0.5;
	
	/**sets up the basic properties of the solution, but doesn't initialise the itemAllocations*/
	private SubProblemBSolution(Graph graph, SubProblemASolution subProblemASolution, Random random, boolean useTailSet) {
		this.random = random;
		this.graph = graph;
		this.subProblemASolution = subProblemASolution;
		this.useTailSet = useTailSet;
		
		this.tripsToRoute = null;
		this.routes = null;
		this.fitness = Double.NaN;
		this.countLoops = 0;
	}
	
	public static SubProblemBSolution generate(Graph graph, SubProblemASolution subProblemASolution, Random random, boolean useTailSet) {
		SubProblemBSolution rval = new SubProblemBSolution(graph, subProblemASolution, random, useTailSet);
		
		// initial ordering is just that in file
		rval.tripsToRoute = subProblemASolution.getItemTrips();
		
		//,hcLoopCount run a bunch of mutations to shuffle...
		for (int i = 0; i < rval.tripsToRoute.size(); i++) {
			rval.tripsToRoute = rval.mutateSwapTrips();
			for (int j = 0; j < 5; j++) {
				rval.tripsToRoute = rval.mutateSwapItemsWithinTrip();
			}
		}
		
		rval.routes = rval.allocateRoutes();
		
		rval.fitness = rval.computeFitness();
		
		return rval;
	}
	
	private double computeFitness() {
		// metric based on running qpptw algorithm
		return (this.totalTime() * fitnessWeightTotalTime);
	}
	
	public double getFitness() {
		return fitness;
	}
	
	/**assumes qpptw algorithm has been run already. Take start/end times of routes and return total running time*/
	public double totalTime() {
		double total = 0;
		for (List<Route> routesForTruck : this.routes.values()) {
			for (Route route : routesForTruck) {
				total += (route.getEndTimeInSeconds() - route.getStartTimeInSeconds());
			}
		}
		
		return total;
	}
	
	private Map<Truck, List<Route>> allocateRoutes() {
		Map<Truck, List<Route>> rval = new HashMap<>();
		
		// create what are effectively duplicates of the graph and components to allow for allocation of time windows
		TimeEstimator te = new TimeEstimator(graph.getTruckSpeed());
		GraphWithTimeWindows graphTW = new GraphWithTimeWindows(this.graph, te, 0, Double.POSITIVE_INFINITY);
		
		QPPTW qpptw = new QPPTW(graphTW, te, this.useTailSet);
		
		for (ItemTrip itemTrip : this.tripsToRoute) {
			List<Route> l = rval.get(itemTrip.getTruck());
			double startTime = 0; // the start time for all trucks is 0
			if (l == null) { // no routes allocated for this truck so far
				l = new ArrayList<>();
				rval.put(itemTrip.getTruck(), l);
			} else { // already got a route allocated, so take the end time, and add on the necessary padding to get the next start time
				Route previousRoute = l.get(l.size() - 1);
				double unloadingCost = this.graph.getTimeCostForUnloadingOneItem() * previousRoute.getItemTrip().getQuantity();
				startTime = previousRoute.getEndTimeInSeconds() + unloadingCost;
			}
			
			Route r = qpptw.allocateRoute(startTime, itemTrip);
			countLoops = qpptw.getCountLoop();
			
			if (r == null) { // problem allocating route!
				System.out.println("WARNING: no route allocated to " + itemTrip);
			}
			
			l.add(r);
		}
		
		return rval;
	}
	
	public int getCountLoops() {
		return countLoops;
	}
	
	/**
	 * takes this {@link SubProblemASolution} and mutates it using one of the mutation operators,
	 * operator is selected probabilistically according to the mutation weights
	 */
	public SubProblemBSolution mutate() {
		// mutation for swapping items within a trip
		// mutation for swapping trips for allocation
		
		// need to update:
		//this.fitness = Double.NaN;
		//this.tripsToRoute = null;
		SubProblemBSolution rval = new SubProblemBSolution(this.graph, this.subProblemASolution, this.random, this.useTailSet);
		
		double totalProbability = mutationProbabilitySwapItemsInTrip + mutationProbabilitySwapTrips;
		double selection = this.random.nextDouble() * totalProbability;
		if (selection < mutationProbabilitySwapItemsInTrip) {
			rval.tripsToRoute = mutateSwapItemsWithinTrip();
		} else {
			rval.tripsToRoute = mutateSwapTrips();
		}
		
		rval.routes = rval.allocateRoutes();

		rval.fitness = rval.computeFitness();
		
		return rval;
	}
	
	// mutation: swap items in jobs
	// find a pair of items allocated to a truck and swap the order in which they'll be picked up
	// it's possible that all the trips have only one destination (one item to get)
	// so for this reason after we've tried all of them we give up and return null to indicate failure
	private List<ItemTrip> mutateSwapItemsWithinTrip() {
		// copy existing ordering
		List<ItemTrip> existingTripItems = new ArrayList<>(this.tripsToRoute);
		
		// choose trips to mutate: keep going until we find one with multiple destinations
		List<Integer> indicesToChoose = new ArrayList<>(existingTripItems.size());
		for (int i = 0; i < existingTripItems.size(); i++) {
			indicesToChoose.add(i);
		}
		
		int tripToSwapInIndex;
		do {
			tripToSwapInIndex = indicesToChoose.remove(this.random.nextInt(indicesToChoose.size()));
			if (existingTripItems.get(tripToSwapInIndex).getItemsToGetGroupedByOrder().size() <= 1) { // if there's only 1 item, we don't want this index
				tripToSwapInIndex = -1;
			}
		} while ((tripToSwapInIndex < 0) && !indicesToChoose.isEmpty());
		
		if (tripToSwapInIndex < 0) {
			return null;
		} else {
			ItemTrip tripToMutate = existingTripItems.get(tripToSwapInIndex);
			List<ItemToGet> itemsToGet = new ArrayList<>(tripToMutate.getItemsToGetUnGrouped());
			
			// pick two indices and swap
			int index1 = this.random.nextInt(itemsToGet.size());
			int index2;
			do {
				index2 = this.random.nextInt(itemsToGet.size());
			} while (index1 == index2);
			
			Collections.swap(itemsToGet, index1, index2);
			
			ItemTrip mutatedTrip = new ItemTrip(itemsToGet, tripToMutate.getTruck());
			existingTripItems.set(tripToSwapInIndex, mutatedTrip);
			
			return existingTripItems;
		}
	}
	
	// mutation: swap jobs
	// find a pair of trips and swap the order in which they'll be allocated routes
	private List<ItemTrip> mutateSwapTrips() {
		// copy existing ordering
		List<ItemTrip> existingTripItems = new ArrayList<>(this.tripsToRoute);
		
		// pick two indices and swap
		int index1 = this.random.nextInt(existingTripItems.size());
		int index2;
		do {
			index2 = this.random.nextInt(existingTripItems.size());
		} while (index1 == index2);
			
		Collections.swap(existingTripItems, index1, index2);
		
		return existingTripItems;
	}
	
	public void copyRoutesToTrucks() {
		for (Truck truck : this.graph.getTrucks().values()) {
			List<Route> routes = this.routes.get(truck);
			if (routes == null) { // that is, if the truck has no routes allocated...
				routes = Collections.emptyList();
			}

			truck.setRoutes(routes);
		}
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer("SubProblemBSolution:[");
		/*
		buf.append(System.lineSeparator());
		for (Entry<Integer, Truck> e : graph.getTrucks().entrySet()) {
			buf.append("Truck ");
			buf.append(e.getKey());
			buf.append(System.lineSeparator());
			if (e.getValue().getRoutes() != null) {
				for (Route r : e.getValue().getRoutes()) {
					buf.append(r.toString());
					buf.append(System.lineSeparator());
				}
			}
		}
		buf.append("]");
		*/
		
		for (ItemTrip t : this.tripsToRoute) {
			buf.append(t);
			buf.append(System.lineSeparator());
		}
		
		return buf.toString();
	}
}
