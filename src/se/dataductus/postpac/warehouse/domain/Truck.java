package se.dataductus.postpac.warehouse.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.dataductus.postpac.warehouse.solution.ItemTrip;
import se.dataductus.postpac.warehouse.solution.qpptw.Route;
import se.dataductus.postpac.warehouse.solution.qpptw.RouteEdgeReservation;
import se.dataductus.postpac.warehouse.solution.qpptw.RouteNodeLoading;
import se.dataductus.postpac.warehouse.solution.qpptw.RouteStep;

/**descriptor of a truck*/
public class Truck {
	private final int id;
	private final int capacity;
	
	/**needed for subproblem B*/
	private List<ItemTrip> trips;
	
	/**result of subproblem B*/
	private List<Route> routes;
	
	public Truck(int id, int capacity) {
		this.id = id;
		this.capacity = capacity;
		this.trips = null;
	}
	
	public Truck(Truck truckToCopy) {
		this(truckToCopy.getId(), truckToCopy.getCapacity());
		this.setTrips(truckToCopy.getTrips());
	}

	public int getId() {
		return id;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public List<ItemTrip> getTrips() {
		return Collections.unmodifiableList(trips);
	}
	
	public void setTrips(List<ItemTrip> trips) {
		this.trips = new ArrayList<>(trips);
	}
	
	public void setRoutes(List<Route> routes) {
		this.routes = new ArrayList<>(routes);
	}
	
	public List<Route> getRoutes() {
		if (routes != null) {
			return Collections.unmodifiableList(routes);
		} else {
			return null;
		}
	}
	
	/**@return 2 element array of x,y coords, or null if not on graph at specified time*/
	public double[] getPositionAtTime(double time) {
		if (this.routes == null) {
			return null;
		} else {
			for (Route route : routes) {
				if ((route.getStartTimeInSeconds() <= time) && (route.getEndTimeInSeconds() >= time)) { // find a route that covers this time period
					// within the route, find a matching edge
					for (RouteStep rs : route.getSteps()) {
						if ((rs.getTimePeriod().containsTime(time))) {
							// how far along edge at this time?
							double start = rs.getTimePeriod().getA();
							double end = rs.getTimePeriod().getB();
							double fraction = (time - start) / (end - start);
							double x, y;
							
							if (rs instanceof RouteEdgeReservation) {
								RouteEdgeReservation er = ((RouteEdgeReservation)rs);
								x = (fraction * (er.getEdge().getNodeTo().getxCoordinate() - er.getEdge().getNodeFrom().getxCoordinate())) + er.getEdge().getNodeFrom().getxCoordinate();
								y = (fraction * (er.getEdge().getNodeTo().getyCoordinate() - er.getEdge().getNodeFrom().getyCoordinate())) + er.getEdge().getNodeFrom().getyCoordinate();
							} else {
								RouteNodeLoading nl = ((RouteNodeLoading)rs);
								x = nl.getNode().getxCoordinate();
								y = nl.getNode().getyCoordinate();
							}
							
							return new double[] {x, y};
						}
					}
				}
			}
			
			// if we get here, we didn't find a route matching the time
			return null;
		}
	}
	
	@Override
	public boolean equals(Object that) {
		return (that instanceof Truck) && (this.getId() == ((Truck)that).getId());
	}
	
	@Override
	public int hashCode() {
		return this.getId();
	}
	
	@Override
	public String toString() {
		return "Truck[id=" + id + ", capacity=" + capacity + ", trips=" + trips + "]";
	}
}
