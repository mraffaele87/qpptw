package se.dataductus.postpac.warehouse.solution;

import gmtools.common.Sets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import se.dataductus.postpac.warehouse.Graph;
import se.dataductus.postpac.warehouse.Tasks;
import se.dataductus.postpac.warehouse.domain.Item;
import se.dataductus.postpac.warehouse.domain.Node;
import se.dataductus.postpac.warehouse.domain.Order;
import se.dataductus.postpac.warehouse.domain.OrderItem;
import se.dataductus.postpac.warehouse.domain.Resource;
import se.dataductus.postpac.warehouse.domain.Truck;
import se.dataductus.postpac.warehouse.domain.loading.Problem;

/**
 * wraps up a solution for sub-problem A: that is, the task of allocating items/locations to trucks
 */
public class SubProblemASolution {
	private final Random random;
	
	/**the static data connected with the problem*/
	private final Graph graph;
	
	/** the list of all trips of items to trucks represented by this solution. */
	private List<ItemTrip> itemTrips;
	
	private Map<Resource, Integer> itemsLeftAtEachResource;
	
	/**a primitive double for the moment*/
	private double fitness;
	
	// weights for fitness
	private static double fitnessWeightTotalDistance = 0.5;
	private static double fitnessWeightAverageMovements = 0.5;
	
	// weights for mutation
	private static double mutationProbabilityChangeTruck = 0.5;
	private static double mutationProbabilityChangeResource = 0.5;
	
	private final int maximumTruckCapacity;
	
	/**sets up the basic properties of the solution, but doesn't initialise the itemTrips*/
	private SubProblemASolution(Graph graph, Random random) {
		this.random = random;
		this.graph = graph;
		this.itemsLeftAtEachResource = new HashMap<>();
		
		// first, how much of each item is there at each resource at the start?
		for (Item item : graph.getItems().values()) {
			for (Resource location : item.getLocations()) {
				this.itemsLeftAtEachResource.put(location, location.getQuantity());
			}
		}
		
		this.fitness = Double.NaN;
		this.itemTrips = null;
		
		int maximumTruckCapacity = 0;
		for (Truck t : graph.getTrucks().values()) {
			maximumTruckCapacity = Math.max(maximumTruckCapacity, t.getCapacity());
		}
		this.maximumTruckCapacity = maximumTruckCapacity;
	}
	
	/**
	 * given the specified problem definition, create a new solution:
	 * a random trip of items to trucks and location
	 */
	public static SubProblemASolution generate(Graph graph, Tasks tasks, Random random) {
		SubProblemASolution rval = new SubProblemASolution(graph, random);

		// the items that need allocated (each contains a list of locations)
		List<Order> orders = tasks.getOrders();
		
		// the trucks that are available
		Map<Integer,Truck> trucks = graph.getTrucks();
		
		// randomly generate set of allocations
		rval.itemTrips = rval.allocateAllItems(orders, trucks);
		
		// copy trips to other stores and track resulting resource levels at each location
		rval.copyTripsToOtherStores();
		
		// finally, assess fitness of this solution
		rval.fitness = rval.computeFitness();
		
		return rval;
	}
	
	/**
	 * @param graph supplied to allow recycling of items and resources
	 * @param tasks supplied to allow recycling of orders
	 * @param random supplied because we'll still need it for mutate() method
	 */
	public static SubProblemASolution readFromProblem(Problem problem, Graph graph, Tasks tasks, Random random) {
		if (problem.getItemTrips() == null) {
			System.err.println("JSON is missing item trip data! Quitting.");
			System.exit(1);
		}
		
		SubProblemASolution rval = new SubProblemASolution(graph, random);
		
		rval.itemTrips = new ArrayList<>();
		for (se.dataductus.postpac.warehouse.domain.loading.ItemTrip ia : problem.getItemTrips()) {
			Map<Integer, List<ItemToGet>> itemsToGetGroupedByOrder = new HashMap<>();
			for (se.dataductus.postpac.warehouse.domain.loading.ItemToGet itg : ia.getItemsToGet()) {
				Item item = graph.getItems().get(itg.getItemID());
				Resource location = null;
				resourceLoop:
				for (Resource r : item.getLocations()) {
					if (r.getId() == itg.getLocationID()) {
						location = r;
						break resourceLoop;
					}
				}
				
				ItemToGet itemToGet = new ItemToGet(item, location, itg.getQuantity(), itg.getOrderID());
				
				List<ItemToGet> itemsToGet = itemsToGetGroupedByOrder.get(itg.getOrderID());
				if (itemsToGet == null) {
					itemsToGet = new ArrayList<>();
					itemsToGetGroupedByOrder.put(itg.getOrderID(), itemsToGet);
				}
				itemsToGet.add(itemToGet);
			}
			
			List<ItemToGet> itemsToGetInTrip = new ArrayList<>();
			for (List<ItemToGet> l : itemsToGetGroupedByOrder.values()) {
				itemsToGetInTrip.addAll(l);
			}
			
			Truck t = graph.getTrucks().get(ia.getTruckID());
			rval.itemTrips.add(new ItemTrip(itemsToGetInTrip, t));
		}
		
		// copy trips to other stores and track resulting resource levels at each location
		rval.copyTripsToOtherStores();
		
		// finally, assess fitness of this solution
		rval.fitness = rval.computeFitness();
		
		return rval;
	}
	
	/**copy trips to other stores and track resulting resource levels at each location*/
	private void copyTripsToOtherStores() {
		for (ItemTrip itemTrip : this.itemTrips) {
			for (ItemToGet itemToGet : itemTrip.getItemsToGetUnGrouped()) {
				Resource resource = itemToGet.getLocation();
				int newQuantity = this.itemsLeftAtEachResource.get(resource).intValue() - itemToGet.getQuantity();
				this.itemsLeftAtEachResource.put(resource, newQuantity);
			}
		}
	}

	/**
	 * here, we want to assign items from orders to trips
	 * all chosen randomly
	 * simplest way is to just iterate over the items, and for each
	 * pick a random truck and fill it up with the items
	 * (simple greedy approach - find the first trip with a big enough space and put the items in it,
	 * or make a new trip if no existing one has enough space)
	 * if the truck isn't big enough, then find more trucks and split the items between them.
	 * we only split trips if no truck exists that's big enough!
	 * 
	 * note: this method does not update the quantity of items at each location, but it does check that the allocations are valid
	 * for the current quantities present
	 */
	private List<ItemTrip> allocateAllItems(List<Order> orders, Map<Integer,Truck> trucks) {
		List<ItemTrip> rval = new ArrayList<>();
		
		// keep track of items remaining at each location within this method
		Map<Resource, Integer> currentItemsLeftAtEachResource = this.itemsLeftAtEachResource;
		
		// for each item, allocate a random location and random place
		for (Order order : orders) {
			// first, do we need to split up the order?
			// probably not needed, so done fairly crudely just in case so we can guarantee that orders will fit into trucks
			List<PartialOrder> partialOrders = new ArrayList<>();
			List<OrderItem> partialOrderItems = new ArrayList<>();
			int numberOfItemsInCurrentPartialOrder = 0;
			for (OrderItem orderItem : order.getItems()) {
				int currentItemQuantity = orderItem.getQuantity();
				
				// while the current item would cause this part of the order to exceed max truck capacity,
				// divide item up between partial orders
				while (numberOfItemsInCurrentPartialOrder + currentItemQuantity > this.maximumTruckCapacity) {
					// if we can, add some of the current item to the current order part
					if (numberOfItemsInCurrentPartialOrder < this.maximumTruckCapacity) {
						OrderItem oi = new OrderItem(orderItem.getItem(), (this.maximumTruckCapacity - numberOfItemsInCurrentPartialOrder));
						partialOrderItems.add(oi);
						currentItemQuantity -= oi.getQuantity();
						
						orderItem = new OrderItem(orderItem.getItem(), currentItemQuantity); // create an orderItem for the remainder of the quantity
					}
					
					partialOrders.add(new PartialOrder(order, partialOrderItems));
					partialOrderItems = new ArrayList<>();
					numberOfItemsInCurrentPartialOrder = 0;
				}
				
				// now add items to current partial orders list
				partialOrderItems.add(orderItem);
				numberOfItemsInCurrentPartialOrder += currentItemQuantity;
			}
			partialOrders.add(new PartialOrder(order, partialOrderItems));
			
			// now allocate those "partial orders" to trucks
			for (PartialOrder partialOrder : partialOrders) {
				// loop over the existing trips, if one has enough space, put the item into that one
				int tripToReplaceIndex = -1;
				tripToReplaceLoop:
				for (int i = 0; i < rval.size(); i++) {
					if (partialOrder.getQuantity() <= rval.get(i).getRemainingTruckCapacity()) {
						tripToReplaceIndex = i;
						break tripToReplaceLoop;
					}
				}
				
				if (tripToReplaceIndex >= 0) { // we've found an existing trip with enough free capacity to take all these items
					ItemTrip oldTrip = rval.get(tripToReplaceIndex);
					List<ItemToGet> itemsInThisTrip = new ArrayList<>(oldTrip.getItemsToGetUnGrouped());

					// where to get them from? might not be enough in one location so we add all locations necessary
					List<ItemToGet> itemsToGetThisPartialOrder = new ArrayList<>();
					for (OrderItem orderItem : partialOrder.getItems()) {
						List<ItemToGet> l = chooseRandomLocation(orderItem.getItem(), orderItem.getQuantity(), partialOrder.getOrder().getId(), currentItemsLeftAtEachResource);
						if (l != null) {
							itemsToGetThisPartialOrder.addAll(l);
						} else { // not enough of this item!
							System.err.println("Not enough of " + orderItem.getItem() + " to fulfil order " + partialOrder.getOrder().getId());
						}
					}
					
					// add the current partial order's items to the existing trip
					itemsInThisTrip.addAll(itemsToGetThisPartialOrder);
					
					ItemTrip newTrip = new ItemTrip(itemsInThisTrip, oldTrip.getTruck());
					rval.set(tripToReplaceIndex, newTrip);
				} else { // no existing trip has enough capacity for all of the items.
					// pick a truck at random
					Truck truck;
					do {
						truck = trucks.get(this.random.nextInt(trucks.size()));
					} while (partialOrder.getQuantity() > truck.getCapacity()); // only pick a large enough truck for the order!
				
					// where to get them from?
					List<ItemToGet> itemsToGetThisPartialOrder = new ArrayList<ItemToGet>();
					for (OrderItem orderItem : partialOrder.getItems()) {
						List<ItemToGet> l = chooseRandomLocation(orderItem.getItem(), orderItem.getQuantity(), partialOrder.getOrder().getId(), currentItemsLeftAtEachResource);
						if (l != null) {
							itemsToGetThisPartialOrder.addAll(l);
						} else { // not enough of this item!
							System.err.println("Not enough of " + orderItem.getItem() + " to fulfil order " + partialOrder.getOrder().getId());
						}
					}
					
					// finally create and add the new trip
					ItemTrip newTrip = new ItemTrip(itemsToGetThisPartialOrder, truck);
					rval.add(newTrip);
				}
			} // end of loop over partial orders
		} // end of loop over orders
		
		return rval;
	}
	
	/**
	 * this allocates multiple locations depending on availability at each one
	 * it'll try to fill the order using the smallest number of locations possible
	 * Note: as a result, in the worst case we try nChoosek locations
	 * @param item to get
	 * @param quantity amount to get
	 * @param orderID the order this item is for (for the ItemToGet objects)
	 * @param quantityAtEachLocation - map storing current quantities of items at each location (this method will not update this) 
	 * @return list of ItemToGets, or null if there just aren't enough items to meet the request
	 */
	private List<ItemToGet> chooseRandomLocation(Item item, int quantity, int orderID, Map<Resource, Integer> quantityAtEachLocation) {
		// randomise order in which we'll do this
		List<Resource> locations = new ArrayList<>(item.getLocations());
		Collections.shuffle(locations, this.random);

		// quick check: are there enough items across all locations?
		int total = 0;
		for (Resource location : locations) {
			total += location.getQuantity();
		}
		if (total < quantity) {
			return null;
		} else {
			for (int numLocations = 1; numLocations <= locations.size(); numLocations++) {
				// loop through the possible locations until we've got all the items we need
				int[][] indices = Sets.nChooseKSets(locations.size(), numLocations);
				
				for (int combination = 0; combination < indices.length; combination++) {
					// can we get all that we need from this set of locations?
					total = 0;;
					for (int i = 0; i < indices[combination].length; i++) {
						total += locations.get(indices[combination][i]).getQuantity();
					}
					
					if (total >= quantity) {
						int amountLeft = quantity;
						List<ItemToGet> rval = new ArrayList<>();
						for (int i = 0; i < indices[combination].length; i++) {
							Resource location = locations.get(indices[combination][i]);
							int amountLeftAtLocation = quantityAtEachLocation.get(location);
							ItemToGet itg = new ItemToGet(item, location, Math.min(amountLeft, amountLeftAtLocation), orderID);
							amountLeft -= location.getQuantity();
							
							rval.add(itg);
						}
						
						return rval;
					}
				}
			}
			
			// in theory, should never get here
			return null;
		}
	}
	
	private double computeFitness() {
		// TODO could replace both with a metric based on running sub problem B
		return (this.averageDistance() * fitnessWeightTotalDistance) + (this.variationInTruckMovementsRequired() * fitnessWeightAverageMovements);
	}
	
	/**assuming shortest paths, what's the average distance between all pairs of locations for each trip?*/
	public double averageDistance() {
		// items from different orders must be on separate runs, but items from same order can be grouped
		// so take all items from one order allocated to a truck, and get the total distance between all
		// pairs within that set. This attempts to estimate journey lengths (the magnitude won't be right,
		// but the idea is that a route with all spread out nodes will be longer than one where they're all
		// together)
		double totalDistance = 0;
		int pairs = 0;
		
		// store total movements for each truck during each trip
		for (ItemTrip itemTrip : this.itemTrips) {
			// for this trip, which nodes are visited? 
			List<Node> nodesVisited = new ArrayList<>();
			for (ItemToGet itemToGet : itemTrip.getItemsToGetUnGrouped()) {
				nodesVisited.add(itemToGet.getLocation().getLocation());
			}
			
			// get total distances for this trip
			for (Node from : nodesVisited) {
				for (Node to : nodesVisited) {
					if (from != to) {
						totalDistance += this.graph.getShortestPathLength(from, to);
						pairs++;
					}
				}
			}
		}
		
		return totalDistance / pairs;
	}
	
	/**
	 * get the variation in the number of truck movements required (just the variance)
	 * we want this to be low so we maximise parallel use of trucks
	 */
	public double variationInTruckMovementsRequired() {
		// items from different orders must be on separate runs, but items from same order can be grouped
		// just take the simplistic approach that the items will be dealt with first-come-first-served
		Map<Integer,Integer> movementsPerTruck = this.truckMovementsRequired();
		
		Integer[] movements = movementsPerTruck.values().toArray(new Integer[movementsPerTruck.size()]);
		return variance(movements);
	}
	
	public Map<Integer,Integer> truckMovementsRequired() {
		Map<Integer,Integer> movementsPerTruck = new HashMap<>();
		for (Integer id : this.graph.getTrucks().keySet()) {
			movementsPerTruck.put(id, 0);
		}
		
		// store total movements for each truck during each order
		for (ItemTrip it : this.itemTrips) {
			int truckID = it.getTruck().getId();
			int currentCount = movementsPerTruck.get(truckID);
			movementsPerTruck.put(truckID, currentCount + 1);
		}
		
		return movementsPerTruck;
	}
	
    private static double variance(Integer[] values) {
        double mean = mean(values);
        double temp = 0;
        for(double a : values) {
            temp += (mean-a)*(mean-a);
        }
        return temp / values.length;
    }
    
    private static double mean(Integer[] values) {
        double sum = 0.0;
        for(double a : values) {
            sum += a;
        }
        return sum / values.length;
    }
	
	/**
	 * takes this {@link SubProblemASolution} and mutates it using one of the mutation operators,
	 * operator is selected probabilistically according to the mutation weights
	 */
	public SubProblemASolution mutate() {
		// need to update:
		//this.fitness = Double.NaN;
		//this.itemTrips = null;
		//this.itemTripsGroupedByOrder = null;
		SubProblemASolution rval = new SubProblemASolution(this.graph, this.random);

		double totalProbability = mutationProbabilityChangeResource + mutationProbabilityChangeTruck;
		double selection = this.random.nextDouble() * totalProbability;
		if (selection < mutationProbabilityChangeResource) {
			rval.itemTrips = mutateTripChangeResource();
		} else {
			rval.itemTrips = mutateTripChangeTruck();
		}

		rval.copyTripsToOtherStores();
		rval.fitness = rval.computeFitness();
		return rval;
	}
	
	/**
	 * moves a group of items (partial order) from one trip to another (may still be using the same truck)
	 * can create a new trip
	 * also deletes existing trip if moving the item out of it leaves no items to collect on that trip
	 */
	private List<ItemTrip> mutateTripChangeTruck() {
		// take trips in this instance, and copy all into a new list
		// pick one at random and swap
		// most of the work here is to do with tidying up afterwards to ensure that we don't
		// end up with multiple trips for the same item in the same place to the same truck
		
		//Map<Integer, List<ItemTrip>> rval = copyItemTripsGroupedByOrder();
		List<ItemTrip> rval = new ArrayList<>(this.itemTrips);
		
		// choose random trip, and a random collection of items for an order within that trip
		int indexTripToRemoveFrom = this.random.nextInt(rval.size());
		ItemsToGetForOrder itemsToGetToMove;
		ItemTrip tripToRemoveFrom;
		boolean removedTrip;
		if (rval.get(indexTripToRemoveFrom).getItemsToGetGroupedByOrder().size() == 1) { // if only 1 partial order, remove the old trip 
			tripToRemoveFrom = rval.remove(indexTripToRemoveFrom);
			itemsToGetToMove = tripToRemoveFrom.getItemsToGetGroupedByOrder().get(0);
			removedTrip = true;
		} else {
			tripToRemoveFrom = rval.get(indexTripToRemoveFrom);
			int itemIndex = this.random.nextInt(tripToRemoveFrom.getItemsToGetGroupedByOrder().size());
			itemsToGetToMove = tripToRemoveFrom.getItemsToGetGroupedByOrder().get(itemIndex);
			
			// also, make a new version of tripToMutate, with the items for the order removed
			List<ItemToGet> itemsWithOrderRemoved = new ArrayList<>(tripToRemoveFrom.getItemsToGetUnGrouped().size());
			for (int i = 0; i < tripToRemoveFrom.getItemsToGetGroupedByOrder().size(); i++) {
				if (i != itemIndex) {
					itemsWithOrderRemoved.addAll(tripToRemoveFrom.getItemsToGetGroupedByOrder().get(i).getItemsToGet());
				}
			}
			
			rval.set(indexTripToRemoveFrom, new ItemTrip(itemsWithOrderRemoved, tripToRemoveFrom.getTruck()));
			removedTrip = false;
		}
		
		// choose a new trip to insert this item into
		// do this by finding all the trips for this truck with enough room to fit this set of items into
		// we also have an equal probability of creating a new trip for the truck
		
		// find the trips with spare capacity
		List<Integer> tripsWithSpaceIndices = new ArrayList<>();
		for (int i = 0; i < rval.size(); i++) {
			ItemTrip existingTrip = rval.get(i);
			if (removedTrip || (i != indexTripToRemoveFrom)) { // don't include the one we've just removed the item from!
				if (existingTrip.getRemainingTruckCapacity() >= itemsToGetToMove.getQuantity()) {
					tripsWithSpaceIndices.add(i);
				}
			}
		}
		
		int indexTripToAddTo = this.random.nextInt(tripsWithSpaceIndices.size() + 1); // +1 adds the possibility of creating a new trip
		if (indexTripToAddTo < tripsWithSpaceIndices.size()) { // add to an existing trip
			ItemTrip newTripToCopy = rval.remove(tripsWithSpaceIndices.get(indexTripToAddTo).intValue());
			
			// if the new trip has already got a visit to the requested resource, then merge
			// no longer do this, so we can keep orders within a truck separate.
			// the merging is now done as part of the ItemTrip class
			//int tripContainsItemToGet = -1;
			/*
			existingItemLoop:
			for (int i = 0; i < newTripToCopy.getItemsToGet().size(); i++) {
				ItemToGet itg = newTripToCopy.getItemsToGet().get(i);
				if (itg.getLocation().equals(itemListToMove.getLocation())) {
					tripContainsItemToGet = i;
					break existingItemLoop;
				}
			}
			*/
			
			List<ItemToGet> existingItemsInTrip = new ArrayList<>(newTripToCopy.getItemsToGetUnGrouped());

			existingItemsInTrip.addAll(itemsToGetToMove.getItemsToGet());
			ItemTrip newTrip = new ItemTrip(existingItemsInTrip, newTripToCopy.getTruck());
			rval.add(newTrip);
		} else { // create a new trip
			// choose a new truck at random, if it's too small, look for a bigger truck
			Truck truck;
			do {
				truck = this.graph.getTrucks().get(this.random.nextInt(this.graph.getTrucks().size()));
			} while (itemsToGetToMove.getQuantity() > truck.getCapacity());
			
			ItemTrip newTrip = new ItemTrip(itemsToGetToMove.getItemsToGet(), truck);
			rval.add(newTrip);
		}
				
		return rval;
	}
	
	/**changes the location that an item is collected from*/
	private List<ItemTrip> mutateTripChangeResource() {
		// take trips in this instance, and copy all into a new list
		// pick one at random and change the location
		// as with mutateTripTruckToTruck(), most of the work here 
		// is to do with tidying up afterwards to ensure that we don't
		// end up with multiple trips for the same resource to the same truck in the same trip
		//Map<Integer, List<ItemTrip>> rval = copyItemTripsGroupedByOrder();
		List<ItemTrip> rval = new ArrayList<>(this.itemTrips);
		
		//Integer[] orderKeys = rval.keySet().toArray(new Integer[rval.size()]);
		
		// choose random Order
		// choose random trip within that Order
		// may have to repeat until we find an trip that has an alternative location with enough resource
		//List<ItemTrip> oldTrips;
		ItemTrip tripToMutate;
		ItemsToGetFromLocation itemToMutate;
		int attemptCount = 0;
		boolean done = false;
		do {
			if (attemptCount++ > rval.size()) { // this limit is here in case there are no orders that can be mutated. It'll give up once we've tried as many times as there are orders
				return rval;
			}
			
			//oldTrips = rval.get(orderKeys[this.random.nextInt(orderKeys.length)]);
			int index = this.random.nextInt(rval.size());
			tripToMutate = rval.get(index);
			
			int itemIndex = this.random.nextInt(tripToMutate.getItemsToGetGroupedByOrder().size());
			itemToMutate = tripToMutate.getItemsToGetGroupedByLocation().get(itemIndex);
			
			// are there alternative locations for this item with enough left?
			alternativeResourceLoop:
			for (Resource resource : itemToMutate.getItem().getLocations()) {
				if (!resource.equals(itemToMutate.getLocation()) // different location
						&& (itemToMutate.getQuantity() <= this.itemsLeftAtEachResource.get(resource).intValue())) { // enough items
					done = true;
					break alternativeResourceLoop;
				}
			}
			
			// once we've found one, remove the original
			if (done) {
				rval.remove(index);
			}
		} while (!done);
		
		// pick a new resource for the trip
		//  - we only look for places that have the same or more of the item than the existing trip,
		//    on the assumption that splitting into visiting multiple locations would result in a poorer schedule
		List<Resource> resources = itemToMutate.getItem().getLocations();
		Resource newResource;
		do {
			newResource = resources.get(this.random.nextInt(resources.size()));
		} while (newResource.equals(itemToMutate.getLocation()) && this.itemsLeftAtEachResource.get(newResource).intValue() >= itemToMutate.getQuantity());
		
		// are any items in the trip which are coming from the same place, if so merge
		// assumes that there'll only be one, because this operation is always performed
		// no longer do this, so we can keep orders within a truck separate.
		// the merging is now done as part of the ItemTrip class 
		/*
		ItemToGet existing = null;
		existingItemLoop:
		for (ItemToGet itg : tripToMutate.getItemsToGet()) {
			if ((itg.getItem().equals(itemToMutate.getItem())) && (itg.getLocation().equals(itemToMutate.getLocation()))) { // assumes that these are all in the same order, which they will be thanks to the data structure
				existing = itg;
				break existingItemLoop;
			}
		}
		
		ItemToGet mutatedItem;
		if (existing != null) {
			mutatedItem = new ItemToGet(itemToMutate.getItem(), newResource, itemToMutate.getQuantity() + existing.getQuantity());
		} else {
			mutatedItem = new ItemToGet(itemToMutate.getItem(), newResource, itemToMutate.getQuantity());
		}
		*/
		
		// now we need to update the existing items in this trip to reflect the new location
		List<ItemsToGetForOrder> oldItemsToGetForOrders = tripToMutate.getItemsToGetGroupedByOrder();
		List<ItemToGet> newItemsToGet = new ArrayList<>();
		for (ItemsToGetForOrder oldItemsToGetForOrder : oldItemsToGetForOrders) {
			if (oldItemsToGetForOrder.getLocations().contains(itemToMutate.getLocation())) { // only both changing the items for this order if any of them come from the resource to be changed 
				for (ItemToGet itemToGet : oldItemsToGetForOrder.getItemsToGet()) {
					if (itemToGet.getLocation().equals(itemToMutate.getLocation())) {
						itemToGet = new ItemToGet(itemToGet.getItem(), newResource, itemToGet.getQuantity(), oldItemsToGetForOrder.getOrderID());
					}
					newItemsToGet.add(itemToGet);
				}
			} else {
				newItemsToGet.addAll(oldItemsToGetForOrder.getItemsToGet());
			}
		}
		
		// create a new trip with the updated item locations
		ItemTrip newTrip = new ItemTrip(newItemsToGet, tripToMutate.getTruck());
		rval.add(newTrip);
		
		return rval;
	}
	
	public double getFitness() {
		return fitness;
	}
	
	/**@return list of item trips represented by this solution. trips are grouped by Order*/
	public List<ItemTrip> getItemTrips() {
		return Collections.unmodifiableList(itemTrips);
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer("SubProblemASolution:[");
		buf.append(System.lineSeparator());
		for (ItemTrip it : itemTrips) {
			buf.append(it.toShortString());
		}
		buf.append("]");
		
		return buf.toString();
	}
	
	public void renderToProblem(Problem problem) {
		List<se.dataductus.postpac.warehouse.domain.loading.ItemTrip> trips = new ArrayList<>();
		for (ItemTrip itemTrip : this.itemTrips) {
			List<se.dataductus.postpac.warehouse.domain.loading.ItemToGet> l = new ArrayList<>();
			for (ItemToGet itemToGet : itemTrip.getItemsToGetUnGrouped()) {
				int itemID = itemToGet.getItem().getId();
				int resourceID = itemToGet.getLocation().getId();
				int quantity = itemToGet.getQuantity();
				int orderID = itemToGet.getOrderID();
				l.add(new se.dataductus.postpac.warehouse.domain.loading.ItemToGet(itemID, resourceID, quantity, orderID));
			}
			int truckID = itemTrip.getTruck().getId();
			
			se.dataductus.postpac.warehouse.domain.loading.ItemTrip ia = new se.dataductus.postpac.warehouse.domain.loading.ItemTrip(l, truckID);
			trips.add(ia);
		}
		
		problem.setItemTrips(trips);
	}
}
