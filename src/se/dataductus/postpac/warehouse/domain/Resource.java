package se.dataductus.postpac.warehouse.domain;


/**
 * The location and quantity of a particular item in the warehouse 
 */
public class Resource {
	private final Item item;
	private int quantity;
	private final int storageLevel;
	private final double timeCost;
	private final Node location;
	private final int id;

	public Resource(int id, Item item, Node n,int initialQuantity, int storageLevel,
			double timeCost) {
		this.id = id;
		this.timeCost = timeCost;
		this.item = item;
		this.quantity = initialQuantity;
		this.storageLevel = storageLevel;
		location = n;
	}
	
	public int getId() {
		return id;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Item getItem() {
		return item;
	}

	public int getStorageLevel() {
		return storageLevel;
	}

	public double getTimeCost() {
		return timeCost;
	}
	
	public Node getLocation() {
		return location;
	}
	
	@Override
	public boolean equals(Object that) {
		return (that instanceof Resource) && this.id == (((Resource)that).id);
	}
	
	@Override
	public int hashCode() {
		return this.id;
	}

	public String toShortString() {
		return "Resource[i=" + item.getId()
				+ ", nd=" + location.getId() + ", lvl=" + storageLevel 
				+ ", timeCost=" + timeCost + "]";
	}
	
	@Override
	public String toString() {
		return "Resource[item=" + item.toShortString() + ", quantity=" + quantity
				+ ", location=" + location.getId() + ", storageLevel=" + storageLevel 
				+ ", timeCost=" + timeCost + "]";
	}
}
