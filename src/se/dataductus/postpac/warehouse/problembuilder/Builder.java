package se.dataductus.postpac.warehouse.problembuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import se.dataductus.postpac.warehouse.Graph;
import se.dataductus.postpac.warehouse.domain.loading.Edge;
import se.dataductus.postpac.warehouse.domain.loading.Node;
import se.dataductus.postpac.warehouse.domain.loading.Problem;
import se.dataductus.postpac.warehouse.domain.loading.Resource;
import se.dataductus.postpac.warehouse.domain.loading.Truck;

public class Builder {
	private Random random = new Random(10);
	private int nextNodeId = 0;
	private int nextEdgeId = 0;
	private int nextTruckId = 0;
	private int nextResourceId = 0;
	private ArrayList<Edge> edges = new ArrayList<>();
	private ArrayList<Node> nodes = new ArrayList<>();
	
	/**not all nodes will have resources at them (e.g. intermediate nodes, base node, junction nodes, so keep track of the ones that can)*/
	private List<Integer> nodesThatCanHaveResources = new ArrayList<>();
	
	/**
	 * lines are groups of edges representing one aisle or avenue in the graph
	 * this stores the node IDs that make up the line
	 */
	private ArrayList<ArrayList<Integer>> lines = new ArrayList<>();
	
	private int numItems;
	private ArrayList<Resource> resources = new ArrayList<>();
	private ArrayList<Truck> trucks = new ArrayList<>();
	private int loadingNodeID = 0;
	private double timeCostForLoadingOneItem = 1;
	private double timeCostForUnloadingOneItem = 1;
	
	/**
	 * Add a node, and bidirectional edge connecting it to an existing node
	 * This node can't contain resources
	 * Used to add base station node
	 */
	public Builder singleNode(int nodeIdToConnectTo, int xCoord, int yCoord, boolean base) {
		Edge e = new Edge(nextEdgeId++, nextNodeId, nodeIdToConnectTo, 1);
		edges.add(e);
		e = new Edge(nextEdgeId++, nodeIdToConnectTo, nextNodeId, 1);
		edges.add(e);
		Node n = new Node(nextNodeId, xCoord, yCoord, base);
		nodes.add(n);
		
		nextNodeId++;
		
		return this;
	}
	
	/**
	 * create the bidirectional lines for either end of the graph and the middle avenue
	 * none of these nodes can contain resources
	 * @param n is the number of nodes to add to this line
	 * @param x is the x-coordinate to associate with this line (for visualisation)
	 */
	public Builder bidirectionalLine(int n, int x){
		ArrayList<Integer> l = new ArrayList<>();
		lines.add(l);
		
		for(int i=0;i<n-1;i++){
			Edge e = new Edge(nextEdgeId++,nextNodeId,nextNodeId+1,1);
			edges.add(e);
			l.add(nextNodeId);
			nodes.add(new Node(nextNodeId, x, i));
			
			e = new Edge(nextEdgeId++,nextNodeId+1,nextNodeId,1);
			edges.add(e);
			
			nextNodeId++;
		}
		l.add(nextNodeId);
		nodes.add(new Node(nextNodeId, x, n-1));
		nextNodeId++;
		return this;
	}
	
	/**
	 * add a unidirectional line to the graph
	 * all these nodes can have resources
	 * @param n is the number of nodes that the line passes through
	 * @param line1 start line to connect with
	 * @param line2 end line to connect with
	 * @param pos1 point on line 1 to connect with (locates the right node)
	 * @param pos2 point on line 2 to connect with (locates the right node)
	 * @param startX is the x-coordinate to associate with the start of this line (for visualisation)
	 * @return this Builder
	 */
	public Builder uniLine(int n,int line1,int line2,int pos1,int pos2, int startX){
		
		ArrayList<Integer> l = new ArrayList<>();
		
		int currentNode = lines.get(line1).get(pos1);
		int n2 = lines.get(line2).get(pos2);
		
		for(int i=0;i<n;i++){
			Edge e = new Edge(nextEdgeId++,currentNode,nextNodeId,1);
			edges.add(e);
			currentNode = nextNodeId;
			nodes.add(new Node(nextNodeId, i+startX, pos1));
			l.add(currentNode);
			nodesThatCanHaveResources.add(currentNode);
			nextNodeId++;
		}
		
		lines.add(l);
		
		Edge e = new Edge(nextEdgeId++,currentNode,n2,1);
		edges.add(e);
		
		return this;
	}
	
	public Builder numItems(int i){
		this.numItems = i;
		return this;
	}
	
	public Builder jumbleResources(int item,int minSources,int maxSources){
		
		int l = maxSources - minSources;
		
		int sourceCount = random.nextInt(l)+minSources;
		
		for(int i=0;i<sourceCount;i++){
			int randLevel = random.nextInt(3);
			int randNode = nodesThatCanHaveResources.get(random.nextInt(nodesThatCanHaveResources.size()));
			Resource r = new Resource(nextResourceId++);
			r.setItem(item);
			r.setLevel(randLevel);
			r.setQuantity(1000000);
			r.setNode(randNode);
			resources.add(r);
		}
				
		return this;
	}
	
	/**
	 * add the bidirectional path running along the outermost shelves
	 * @param line1 - line to connect to at start of path
	 * @param line2 - line to connect to at end of path
	 * @param pos1 - position on line1 to connect to
	 * @param pos2 - position on line2 to connect to
	 * @param l - length
	 * @param interval - if >0 and <l, nodes will be added along the two edges edge at the specified separation
	 * @param startX - x coord of first node
	 * @param y - y coord of all nodes in line
	 * @param offset - amount to offset node pairs by
	 * @return
	 */
	public Builder bidirectionalConnection(int line1,int line2, int pos1,int pos2,double l,double interval, double startX, double y, double offset){
		int nid1 = lines.get(line1).get(pos1);
		int nid2 = lines.get(line2).get(pos2);
		
		if ((interval > 0) && (interval < l)) { // if adding intermediate nodes
			int numberToAdd = (int)Math.ceil((l / interval)) - 1;
			double lengthSoFar = 0;

			int from1 = nid1;
			int from2 = nid1;
			
			for (int i = 0; i < numberToAdd; i++) {
				double newNodeX = startX + (interval * (i + 1));
				Node n1 = new Node(nextNodeId++, newNodeX, y - offset);
				nodes.add(n1);
				edges.add(new Edge(nextEdgeId++,from1,n1.getId(),interval));
				from1 = n1.getId();
				
				Node n2 = new Node(nextNodeId++, newNodeX, y + offset);
				nodes.add(n2);
				edges.add(new Edge(nextEdgeId++,n2.getId(),from2,interval));
				from2 = n2.getId();
				
				lengthSoFar = numberToAdd * interval;
			}
			
			// add last edge
			edges.add(new Edge(nextEdgeId++,from1,nid2,(l - lengthSoFar)));
			edges.add(new Edge(nextEdgeId++,nid2,from2,(l - lengthSoFar)));
		} else { // no intermediate nodes, just add edges
			edges.add(new Edge(nextEdgeId++,nid1,nid2,l));
			edges.add(new Edge(nextEdgeId++,nid2,nid1,l));
		}
		
		return this;
	}
	
	public Builder trucks(int capacity, int count){
		for (int i = 0; i < count; i++) {
			trucks.add(new Truck(nextTruckId++, capacity));
		}
		
		return this;
	}
	
	public Problem render(){
		Problem p = new Problem();
		p.setEdges(edges);
		p.setNodes(nodes);
		p.setItemIdRange(numItems);
		//p.setLevelCosts(new double[]{1,3,5}); // was this for tests
		p.setLevelCosts(new double[]{30,180,300}); // for paper
		p.setTruckSpeed(1);
		p.setMinimiumSeparation(0.5);
		p.setItemNames(new HashMap<Integer,String>());
		p.setResources(resources);
		p.setTrucks(trucks);
		p.setMixedItemFixedCost(5);
		p.setLoadingNodeID(loadingNodeID);
		p.setTimeCostForLoadingOneItem(timeCostForLoadingOneItem);
		p.setTimeCostForUnloadingOneItem(timeCostForUnloadingOneItem);
		return p;
	}
	
	public static void main(String[] args){
		// full problem has 7 rows of 56 nodes
		// plus navigation rows at the ends (says 24 but could be 14?)
		// did read somewhere that there was an aisle in the middle too, not in most recent doc though
		
		// make the tracks for the ends of the rows and the middle avenue
		// these have 9 nodes (7 rows, plus the ends for the perimeter)
		Builder b = new Builder().bidirectionalLine(9, 0).bidirectionalLine(9, 36+1).bidirectionalLine(9, 36+20+2);
		
		// make the tracks running along the shelves
		// there are two for each shelf thanks to the middle avenue
		for(int i=1;i<8;i++){
			b.uniLine(36, 0, 1, i, i, 1);
			b.uniLine(20, 1, 2, i, i, 38);
		}
		
		// bidirectional tracks along ends of shelves
		b.bidirectionalConnection(0, 1, 0, 0, 37, 1, 0, 0, 0.2);
		b.bidirectionalConnection(0, 1, 8, 8, 37, 1, 0, 8, 0.2);
		b.bidirectionalConnection(1, 2, 0, 0, 21, 1, 37, 0, 0.2);
		b.bidirectionalConnection(1, 2, 8, 8, 21, 1, 37, 8, 0.2);
		
		// add base node
		b.loadingNodeID = b.nextNodeId;
		b.singleNode(0, -1, -1, true);
		
		//int numItems = 5000; for the GECCO paper
		int numItems = 10000;
		b.numItems(numItems);
		
		for(int i=0;i<numItems;i++){
			b.jumbleResources(i, 2, 5);
		}
		
		b.trucks(10, 4);
		b.trucks(4, 6);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		String s = gson.toJson(b.render());
		//String s = gson.toJson(b.render().getNodes());
		
		System.out.println(s);
		
		// =============
		Problem p = new Gson().fromJson(s, Problem.class);
		Graph g = new Graph(p);
		g.renderGraphInJGraphX();
		g.renderGraphInJGraphX("graphForJnlPaper.png");
	}
}
