package se.dataductus.postpac.warehouse.solution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.dataductus.postpac.warehouse.domain.Item;
import se.dataductus.postpac.warehouse.domain.Resource;

/**A specific item for a truck to get, with the location it'll be got from*/
public class ItemToGet {
	private final Item item;
	private final Resource location;
	private final int quantity;
	private final int orderID;
	
	public ItemToGet(Item item, Resource location, int quantity, int orderID) {
		this.item = item;
		this.location = location;
		this.quantity = quantity;
		this.orderID = orderID;
	}
	
	public Item getItem() {
		return item;
	}
	
	public Resource getLocation() {
		return location;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public int getOrderID() {
		return orderID;
	}
	
	public double getTimeCostForLoading() {
		return quantity * location.getTimeCost();
	}
	
	@Override
	public String toString() {
		return "ItemToGet[item=" + item.toShortString() + ", location=" + location.toString() + ", qty=" + quantity + ", orderID=" + orderID + " ]";
	}

	public static Map<Resource, List<ItemToGet>> groupByLocation(List<ItemToGet> itemsToGet) {
		Map<Resource, List<ItemToGet>> rval = new HashMap<>();
		
		for (ItemToGet itemToGet : itemsToGet) {
			List<ItemToGet> l = rval.get(itemToGet.getLocation());
			if (l == null) {
				l = new ArrayList<>();
				rval.put(itemToGet.getLocation(), l);
			}

			l.add(itemToGet);
		}
		
		return rval;
	}
	
	public static Map<Integer, List<ItemToGet>> groupByOrderID(List<ItemToGet> itemsToGet) {
		Map<Integer, List<ItemToGet>> rval = new HashMap<>();
		
		for (ItemToGet itemToGet : itemsToGet) {
			List<ItemToGet> l = rval.get(itemToGet.getOrderID());
			if (l == null) {
				l = new ArrayList<>();
				rval.put(itemToGet.getOrderID(), l);
			}

			l.add(itemToGet);
		}
		
		return rval;
	}
}
