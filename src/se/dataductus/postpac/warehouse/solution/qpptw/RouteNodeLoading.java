package se.dataductus.postpac.warehouse.solution.qpptw;

import java.util.List;

import se.dataductus.postpac.warehouse.domain.Truck;
import se.dataductus.postpac.warehouse.solution.ItemToGet;

/**
 * a period during which a truck sits on a node, loading
 */
public class RouteNodeLoading extends RouteStep {
	private QPPTWNode node;
	private List<ItemToGet> items;
	
	public RouteNodeLoading(QPPTWNode node, TimePeriod timePeriod, List<ItemToGet> items, Truck truck) {
		super(timePeriod, truck);
		this.node = node;
		this.items = items;
	}
	
	public QPPTWNode getNode() {
		return node;
	}
	
	public List<ItemToGet> getItems() {
		return items;
	}
	
	/**
	 * the time period is the time of arrival at the node to the time of departure
	 * (well, actually the time we start loading to the time we're done - if there are
	 * two items to get from different levels on the same node then between them there 
	 * won't be leaving/arriving)
	 */
	public TimePeriod getTimePeriod() {
		return super.getTimePeriod();
	}
	
	@Override
	public String toString() {
		return "NL[" + this.node.getUnderlyingNode().getId() + "|" + this.getTimePeriod().getA() + "-" + this.getTimePeriod().getB() + "]";
	}
}