package se.dataductus.postpac.warehouse.solution;

import java.util.List;

import se.dataductus.postpac.warehouse.domain.Order;
import se.dataductus.postpac.warehouse.domain.OrderItem;

/**
 * wraps up a list of items from a particular order, small enough to fit into at least one of the trucks
 * mostly, this should be a whole order
 * */
public class PartialOrder {
	private final Order order;
	private final List<OrderItem> items;
	private final int quantity;
	
	public PartialOrder(Order order, List<OrderItem> items) {
		this.order = order;
		this.items = items;
		
		int quantity = 0;
		for (OrderItem oi : items) {
			quantity += oi.getQuantity();
		}
		this.quantity = quantity;
	}
	
	public List<OrderItem> getItems() {
		return items;
	}
	
	public Order getOrder() {
		return order;
	}
	
	public int getQuantity() {
		return quantity;
	}
}