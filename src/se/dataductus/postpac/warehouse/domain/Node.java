package se.dataductus.postpac.warehouse.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node {
	
	private final int id;
	private final List<Edge> in;
	private final List<Edge> out;
	
	private final boolean base;

	/**keys are item IDs, values are lists of Resources*/
	private final Map<Integer,List<Resource>> resources;
	
	// these two are just for visualisation
	private final double x;
	private final double y;

	public Node(int id){
		this(id, 0, 0, false);
	}
	
	public Node(int id, double x, double y, boolean base){
		this.id = id;
		in = new ArrayList<Edge>();
		out = new ArrayList<Edge>();
		resources = new HashMap<>();
		this.x = x;
		this.y = y;
		this.base = base;
	}
	
	public int getId() {
		return id;
	}

	public List<Edge> getIn() {
		return in;
	}

	public List<Edge> getOut() {
		return out;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	public Map<Integer, List<Resource>> getResources() {
		return resources;
	}
	
	public boolean isBase() {
		return base;
	}
	
	@Override
	public boolean equals(Object that) {
		return (that instanceof Node) && (this.getId() == ((Node)that).getId());
	}
	
	@Override
	public int hashCode() {
		return this.getId();
	}
	
	public void addResource(Resource r){
		if(resources.get(r.getItem().getId()) == null){
			resources.put(r.getItem().getId(), (List<Resource>)new ArrayList<Resource>());
		}
			
		resources.get(r.getItem().getId()).add(r);
	}
	
	@Override
	public String toString() {
		return "Node [id=" + id + ", in=" + in + ", out=" + out
				+ ", resources=" + resources + "]";
	}
	
}
