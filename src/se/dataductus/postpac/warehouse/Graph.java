package se.dataductus.postpac.warehouse;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.Timer;

import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;

import se.dataductus.postpac.warehouse.domain.Edge;
import se.dataductus.postpac.warehouse.domain.Item;
import se.dataductus.postpac.warehouse.domain.Node;
import se.dataductus.postpac.warehouse.domain.Resource;
import se.dataductus.postpac.warehouse.domain.Truck;
import se.dataductus.postpac.warehouse.domain.loading.Problem;
import se.dataductus.postpac.warehouse.solution.ItemToGet;
import se.dataductus.postpac.warehouse.solution.qpptw.Route;
import se.dataductus.postpac.warehouse.solution.qpptw.RouteEdgeReservation;
import se.dataductus.postpac.warehouse.solution.qpptw.RouteNodeLoading;
import se.dataductus.postpac.warehouse.solution.qpptw.RouteStep;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

public class Graph {
	private Map<Integer,Node> nodes = new HashMap<>();
	private Set<Edge> edges = new HashSet<>();
	private Map<Integer,Item> items= new HashMap<>();
	private Map<Integer,Truck> trucks = new HashMap<>();
	
	/**this allows us to use the JGraphT algs for e.g. graph traversal*/
	private DirectedWeightedMultigraph<Node,JGraptTEdgeWrapper> graph;
	
	/**identifies the base - where trucks take items to*/
	private Node loadingNode;
	
	/**the time cost associated with loading per item*/
	private double timeCostForLoadingOneItem;

	/**the time cost associated with unloading per item*/
	private double timeCostForUnloadingOneItem;
	
	private double truckSpeed;
	private double minimumSeparation;
	
	/**this switches to true if we found some nodes in the json problem definition - used to determine how to try visualising*/
	private boolean nodesPredefined = false;
	
	@SuppressWarnings("unused")
	private Problem originalProblem;
	// routes
	
	// we cache these to speed things up
	private ShortestPathCache shortestPathCache;
	
	public Graph() {
		this.shortestPathCache = new ShortestPathCache();
	}
	
	/**create a Graph outr of the given Problem instance*/
	public Graph(Problem p) {
		this.shortestPathCache = new ShortestPathCache();
		prepare(p);
	}
	
	public Map<Integer,Item> getItems(){
		return items;
	}
	
	public Map<Integer, Truck> getTrucks() {
		return trucks;
	}
	
	public Collection<Node> getNodes() {
		return this.nodes.values();
	}
	
	public Set<Edge> getEdges() {
		return edges;
	}
	
	public Node getLoadingNode() {
		return loadingNode;
	}
	
	public double getTimeCostForLoadingOneItem() {
		return timeCostForLoadingOneItem;
	}
	
	public double getTimeCostForUnloadingOneItem() {
		return timeCostForUnloadingOneItem;
	}
	
	public double getTruckSpeed() {
		return truckSpeed;
	}
	
	public double getMinimumSeparation() {
		return minimumSeparation;
	}
	
	private void prepare(Problem loadedProblem){
		this.originalProblem = loadedProblem;
			
		// items first
		for(int i=0;i<loadedProblem.getItemIdRange();i++){
			if(loadedProblem.getItemNames().containsKey(i)){
				Item item = new Item(i,loadedProblem.getItemNames().get(i));
				items.put(i, item);
			}
			else{
				items.put(i, new Item(i));
			}
		}
		
		// SB - add nodes before edges if any are defined (this will pick up coords from json)
		if (loadedProblem.getNodes() != null) {
			for(se.dataductus.postpac.warehouse.domain.loading.Node n : loadedProblem.getNodes()){
				nodes.put(n.getId(), new Node(n.getId(), n.getX(), n.getY(), n.isBase()));
				nodesPredefined = true; // if any predefined nodes found, make this true
			}
		}
		
		// edges second
		for(se.dataductus.postpac.warehouse.domain.loading.Edge e : loadedProblem.getEdges()){
			// try find nodes, or create
			tryCreateNode(e.getFrom());
			tryCreateNode(e.getTo());
			
			// get nodes
			Node n1 = nodes.get(e.getFrom());
			Node n2 = nodes.get(e.getTo());
			double l = e.getLength();
			double c = l / loadedProblem.getTruckSpeed();
			Edge finalEdge = new Edge(e.getId(),n1,n2,l,c);
			n1.getOut().add(finalEdge);
			n2.getIn().add(finalEdge);
			this.edges.add(finalEdge);
		}
		
		// trucks
		for (se.dataductus.postpac.warehouse.domain.loading.Truck t : loadedProblem.getTrucks()) {
			Truck truck = new Truck(t.getId(), t.getCapacity());
			this.trucks.put(truck.getId(), truck);
		}
		
		// static problem data
		this.loadingNode = nodes.get(loadedProblem.getLoadingNodeID());
		this.timeCostForLoadingOneItem = loadedProblem.getTimeCostForLoadingOneItem();
		this.timeCostForUnloadingOneItem = loadedProblem.getTimeCostForUnloadingOneItem();
		this.truckSpeed = loadedProblem.getTruckSpeed();
		this.minimumSeparation = loadedProblem.getMinimiumSeparation();
		
		// resources last
		loadResources(loadedProblem);
		
		// copy data into internal Graph object
		updateGraph();
	}
	
	private void updateGraph() {
		this.graph = new DirectedWeightedMultigraph<>(JGraptTEdgeWrapper.class);
		
		for (Node node : this.nodes.values()) {
			this.graph.addVertex(node);
		}
		
		for (Edge edge : this.edges) {
			JGraptTEdgeWrapper jedge = new JGraptTEdgeWrapper(edge);
			this.graph.addEdge(edge.getFrom(), edge.getTo(), jedge);
			this.graph.setEdgeWeight(jedge, edge.getTimeCost());
		}
	}

	private void tryCreateNode(int i){
		if(!nodes.containsKey(i)){
			nodes.put(i, new Node(i)); // if we didn't get the node from the json, we don't have x/y coords defined
		}
	}
	
	private void loadResources(Problem loadedProblem){
		for(se.dataductus.postpac.warehouse.domain.loading.Resource r : loadedProblem.getResources()){
			int id = r.getId();
			Item i = items.get(r.getItemId());
			tryCreateNode(r.getNode());
			Node n = nodes.get(r.getNode());
			int q = r.getQuantity();
			int l = r.getLevel();
			double t = loadedProblem.getLevelCosts()[l]; // + mixed;
			
			Resource finalResource = new Resource(id, i, n, q, l, t);
			
			n.addResource(finalResource);
			i.getLocations().add(finalResource);
		}
	}
	
	public String renderGraphViz(){
		StringBuffer sb = new StringBuffer();
		sb.append("digraph{\n");
		
		HashSet<String> edgeStrings = new HashSet<>();
		HashSet<String> nodeStrings = new HashSet<>();
		
		for(Node n : nodes.values()){
			
			for(Edge e : n.getIn()){
				String k = "    n"+e.getFrom().getId()+"->n"+e.getTo().getId();
				
				if(!biEdge(e.getFrom(),e.getTo())){
					k+=" [weight = 56]";
				}
				k+=";\n";
				
				edgeStrings.add(k);
			}
			for(Edge e : n.getOut()){
				String k = "    n"+e.getFrom().getId()+"->n"+e.getTo().getId();
				if(!biEdge(e.getFrom(),e.getTo())){
					k+=" [weight = 56]";
				}
				k+=";\n";
				edgeStrings.add(k);
			}
			nodeStrings.add("    n"+n.getId()+";\n");
		}
		for(String l : nodeStrings){
			sb.append(l);
		}
		for(String l : edgeStrings){
			sb.append(l);
		}
		
		sb.append("}\n");
		
		
		return sb.toString();
	}
	
	public boolean biEdge(Node n1,Node n2){
		
		boolean a = false;
		boolean b = false;
		
		for(Edge e : n1.getOut()){
			if(e.getFrom().getId() == n1.getId() && e.getTo().getId() == n2.getId()){
				a = true;
			}
		}
		
		for(Edge e : n1.getIn()){
			if(e.getFrom().getId() == n2.getId() && e.getTo().getId() == n1.getId()){
				b = true;
			}
		}
		
		return a && b;
	}
	
	public List<Edge> getShortestPath(Node from, Node to) {
		return this.shortestPathCache.getShortestPath(from, to);
	}
	
	public double getShortestPathTimeCost(Node from, Node to) {
		return this.shortestPathCache.getShortestPathTimeCost(from, to);
	}

	public double getShortestPathLength(Node from, Node to) {
		return this.shortestPathCache.getShortestPathLength(from, to);
	}
	
	/**copy route information in trucks back to original problem instance, ready for writing out to JSON*/
	public void renderTrucksToProblem(Problem problem) {
		List<se.dataductus.postpac.warehouse.domain.loading.Truck> lt = new ArrayList<>();
		for (Entry<Integer, Truck> e : this.trucks.entrySet()) {
			List<se.dataductus.postpac.warehouse.domain.loading.Route> lr = new ArrayList<>();
			
			for (Route route : e.getValue().getRoutes()) {
				List<se.dataductus.postpac.warehouse.domain.loading.NodeVisit> lnv = new ArrayList<>();
				
				// time to leave start node
				RouteStep startStep = route.getSteps().get(0);
				int startNodeID = (startStep instanceof RouteEdgeReservation) ? ((RouteEdgeReservation)startStep).getEdge().getNodeFrom().getUnderlyingNode().getId() : ((RouteNodeLoading)startStep).getNode().getUnderlyingNode().getId();
				se.dataductus.postpac.warehouse.domain.loading.NodeVisit nv = new se.dataductus.postpac.warehouse.domain.loading.NodeVisit(startNodeID, startStep.getTimePeriod().getA());
				lnv.add(nv);
				
				// now add times for all nodes
				for (RouteStep rs : route.getSteps()) {
					// if it's an edge, just add an instantaneous pass through the target node
					if (rs instanceof RouteEdgeReservation) {
						RouteEdgeReservation er = ((RouteEdgeReservation)rs);
						nv = new se.dataductus.postpac.warehouse.domain.loading.NodeVisit(er.getEdge().getNodeTo().getUnderlyingNode().getId(), er.getTimePeriod().getB());
					} else { // otherwise, add a loading period to the node
						RouteNodeLoading nl = ((RouteNodeLoading)rs);
						List<se.dataductus.postpac.warehouse.domain.loading.ItemToGet> li = new ArrayList<>();
						for (ItemToGet itemToGet : nl.getItems()) {
							li.add(new se.dataductus.postpac.warehouse.domain.loading.ItemToGet(itemToGet.getItem().getId(), itemToGet.getLocation().getLocation().getId(), itemToGet.getQuantity(), itemToGet.getOrderID()));
						}
						nv = new se.dataductus.postpac.warehouse.domain.loading.NodeVisit(nl.getNode().getUnderlyingNode().getId(), rs.getTimePeriod().getA(), rs.getTimePeriod().getB(), li);
					}
					
					lnv.add(nv);
				}
				
				se.dataductus.postpac.warehouse.domain.loading.Route r = new se.dataductus.postpac.warehouse.domain.loading.Route(lnv);
				lr.add(r);
			}
			
			se.dataductus.postpac.warehouse.domain.loading.Truck t = new se.dataductus.postpac.warehouse.domain.loading.Truck(e.getValue().getId(), e.getValue().getCapacity(), lr);
			lt.add(t);
		}
		
		problem.setTrucks(lt);
	}
	
	public void renderGraphInJGraphX() {
		renderGraphInJGraphX(Double.NaN, Double.NaN, Double.NaN, 0, null);
	}
	
	public void renderGraphInJGraphX(final String filename) {
		renderGraphInJGraphX(Double.NaN, Double.NaN, Double.NaN, 0, filename);
	}
	
	public void renderGraphInJGraphX(final double startTime, final double endTime, final double incrementS, final int refreshMS) {
		renderGraphInJGraphX(startTime, endTime, incrementS, refreshMS, null);
	}
	
	/**
	 * specify params to have truck positions rendered; leave as NaNs if you don't
	 * @param startTime
	 * @param endTime
	 * @param incrementS
	 * @param refreshMS
	 * @param filename - write out image to file instead of jframe
	 */
	public void renderGraphInJGraphX(final double startTime, final double endTime, final double incrementS, final int refreshMS, final String filename) {
		// we'll change the borders of nodes in the file too; this makes the figure clearer for the paper
		boolean toFile = ((filename != null) && (!filename.isEmpty()));
			
		int frameWidth = 1200;
		int frameHeight = 800;
		
		// this figures are about right, but up to personal preference
		double scale = Math.sqrt(nodes.size()) / (0.8 * Math.sqrt(418)); // use this to scale according to size of graph (418 is node count in full problem)
		int drawingAreaWidth = (int)(3000 * scale);
		int drawingAreaHeight = (int)(800 * scale);
		int frameBorder = 30;
		int nodeWidth = 30;
		int nodeHeight = 30;
		
		mxGraph mxg = new mxGraph();
		Object defaultParent = mxg.getDefaultParent();

		String nodeStyleEmpty = "NodeStyleEmpty";
		Hashtable<String, Object> styleEmpty = new Hashtable<>();
		styleEmpty.put(mxConstants.STYLE_FILLCOLOR, (toFile ? mxUtils.getHexColorString(new Color(0, 0, 170)) : mxUtils.getHexColorString(Color.WHITE)));
		styleEmpty.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
		styleEmpty.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(new Color(0, 0, 170)));
		styleEmpty.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
		styleEmpty.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);
		mxStylesheet stylesheet = mxg.getStylesheet();
		stylesheet.putCellStyle(nodeStyleEmpty, styleEmpty);
		
		String nodeStyleWithResources = "NodeStyleWithResources";
		Hashtable<String, Object> styleWithResources = new Hashtable<>(styleEmpty);
		styleWithResources.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(Color.YELLOW));
		if (toFile) {
			styleWithResources.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.YELLOW));
		}
		stylesheet.putCellStyle(nodeStyleWithResources, styleWithResources);
		
		String nodeStyleBase = "NodeStyleBase";
		Hashtable<String, Object> styleBase = new Hashtable<>(styleEmpty);
		styleBase.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(Color.RED));
		if (toFile) {
			styleBase.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.RED));
		}
		stylesheet.putCellStyle(nodeStyleBase, styleBase);
		
		mxg.getModel().beginUpdate();
		
		double minX = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		for (Node n : this.nodes.values()) {
			minX = Math.min(n.getX(), minX);
			maxX = Math.max(n.getX(), maxX);
			minY = Math.min(n.getY(), minY);
			maxY = Math.max(n.getY(), maxY);
		}
		
		Map<Node, Object> vertices = new HashMap<>();
		for (Node n : nodes.values()) {
			int x, y;
			if (nodesPredefined) {
				int[] coords = convertRowColToPixels(frameBorder, drawingAreaWidth, drawingAreaHeight, minX, maxX, minY, maxY, n.getX(), n.getY());
				x = coords[0];
				y = coords[1];
			} else {
				x = frameWidth / 2;
				y = frameHeight / 2;
			}
			
			String style = (n.getResources().isEmpty()) ? nodeStyleEmpty : nodeStyleWithResources;
			if (n.isBase()) {
				style = nodeStyleBase;
			}
			Object o = mxg.insertVertex(defaultParent, Integer.toString(n.getId()), n.getId(), x, y, nodeWidth, nodeHeight, style);
			vertices.put(n, o);
		}
		
		for (Edge e : edges) {
			mxg.insertEdge(defaultParent, e.toString(), e.getTimeCost(), vertices.get(e.getFrom()), vertices.get(e.getTo()));
		}
		
		mxg.getModel().endUpdate();
		
		final GraphComponentWithTrucks graphComponent = new GraphComponentWithTrucks(mxg, 0, frameBorder, drawingAreaWidth, drawingAreaHeight, minX, maxX, minY, maxY);
		
		mxg.setCellsEditable(false);   // Want to edit the value of a cell in the graph?
		mxg.setCellsMovable(true);    // Moving cells in the graph. Note that an edge is also a cell.
		mxg.setCellsResizable(false);  // Inhibit cell re-sizing.
		mxg.setCellsSelectable(false); // Now I can't even select the cells!!!
		mxg.setEnabled(true); // Catch-All: no interaction with the graph.
		graphComponent.setConnectable(false); // Inhibit edge creation in the graph.

		if (!nodesPredefined) {
			// no coordinates for nodes, so we use a layout - this one seems to work okay
			new mxHierarchicalLayout(mxg).execute(mxg.getDefaultParent());
		}
		
		if (toFile) {
			BufferedImage image = mxCellRenderer.createBufferedImage(mxg, null, 1, Color.WHITE, true, null);
			try {
				ImageIO.write(image, "PNG", new File(filename));
			} catch (IOException e) {
				System.err.print("Problem writing image to " + filename);
				e.printStackTrace();
			}
		} else {
			JFrame frame = new JFrame();
			JScrollPane scrollPane = new JScrollPane(graphComponent);
			frame.getContentPane().add(scrollPane);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(frameWidth, frameHeight);//.pack();
			frame.setVisible(true);
			
			if (!Double.isNaN(startTime) && !Double.isNaN(endTime) && !Double.isNaN(incrementS) && (refreshMS > 0)) {
				Timer timer = new Timer(refreshMS, new ActionListener() {
		            public void actionPerformed(ActionEvent e) {
		                graphComponent.incrementTime(incrementS);
		                graphComponent.repaint();
		            }
		        });
				timer.start();
			}
		}
	}
	
	private static int[] convertRowColToPixels(int frameBorder, int drawingAreaWidth, int drawingAreaHeight, double minX, double maxX, double minY, double maxY, double x, double y) {
		int pixX = frameBorder + (int)(((drawingAreaWidth - (2 * frameBorder)) * ((double)(x - minX) / (maxX - minX))));
		int pixY = frameBorder + (int)(((drawingAreaHeight - (2 * frameBorder)) * ((double)(y - minY) / (maxY - minY))));
		
		return new int[] {pixX, pixY};
	}
	
	/**needed to render trucks on the graph*/
	private class GraphComponentWithTrucks extends mxGraphComponent {
		private static final long serialVersionUID = 1496803068752197082L;

		private double time;
		private int frameBorder;
		private int drawingAreaWidth;
		private int drawingAreaHeight;
		private double minX;
		private double maxX;
		private double minY;
		private double maxY;
		
		public GraphComponentWithTrucks(mxGraph mxGraph, double time, int frameBorder, int drawingAreaWidth, int drawingAreaHeight, double minX, double maxX, double minY, double maxY) {
			super(mxGraph);
			this.time = time;
			this.frameBorder = frameBorder;
			this.drawingAreaWidth = drawingAreaWidth;
			this.drawingAreaHeight = drawingAreaHeight;
			this.minX = minX;
			this.maxX = maxX;
			this.minY = minY;
			this.maxY = maxY;
		}
		
		public void incrementTime(double increment) {
			this.time += increment;
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			g.drawString("Time:" + new DecimalFormat( "0.00" ).format(time), 10, 10);
			
			for (Truck truck : Graph.this.trucks.values()) {
				double[] coords = truck.getPositionAtTime(time);
				if (coords != null) {
					int[] pixelCoords = convertRowColToPixels(frameBorder, drawingAreaWidth, drawingAreaHeight, minX, maxX, minY, maxY, coords[0], coords[1]);
					g.setColor(Color.RED);
					g.fillRect(pixelCoords[0], pixelCoords[1], 25, 25);
					g.setColor(Color.WHITE);
					g.setFont(g.getFont().deriveFont(20.0f));
					g.drawString("" + truck.getId(), pixelCoords[0], pixelCoords[1]+g.getFont().getSize());
				}
			}
		}
	}
	
	/**needed to add Edge objects to graph*/
	private class JGraptTEdgeWrapper extends DefaultWeightedEdge {
		/**courtesy of Eclipse*/
		private static final long serialVersionUID = 2932200133614178656L;
		
		private Edge edge;
		
		public JGraptTEdgeWrapper(Edge edge) {
			this.edge = edge;
		}
		
		public Edge getEdge() {
			return edge;
		}
	}
	
	private class ShortestPathCache {
		/**keys are both node ID: the outer one is the "from"*/
		private Map<Integer,Map<Integer, SPEntry>> cache;
		
		public ShortestPathCache() {
			this.cache = new HashMap<>();
		}
		
		public List<Edge> getShortestPath(Node from, Node to) {
			SPEntry e = getEntry(from, to);
			return e.getShortestPath();
		}

		public double getShortestPathLength(Node from, Node to) {
			SPEntry e = getEntry(from, to);
			return e.getShortestPathLength();
		}
		
		public double getShortestPathTimeCost(Node from, Node to) {
			SPEntry e = getEntry(from, to);
			return e.getShortestPathTimeCost();
		}
		
		private SPEntry getEntry(Node from, Node to) {
			int fromID = from.getId();
			int toID = to.getId();
			
			Map<Integer, SPEntry> m = this.cache.get(fromID);
			SPEntry n = null;
			if (m != null) {
				n = m.get(toID);
				
				if (n != null) {
					return n;
				}
			} else {
				m = new HashMap<Integer, SPEntry>();
				this.cache.put(fromID, m);
			}
			
			// only get here if there was no entry for this pair of nodes in the cache
			List<JGraptTEdgeWrapper> shortestPath;
			
			if (fromID == toID) {
				shortestPath = new ArrayList<>(); // if from and to are the same node, create an empty (zero length) path
			} else {
				DijkstraShortestPath<Node, JGraptTEdgeWrapper> dsp = new DijkstraShortestPath<>(Graph.this.graph, from, to);
				shortestPath = dsp.getPathEdgeList();
			}
			
			List<Edge> edges = new ArrayList<>();
			
			if (shortestPath != null) { // a path exists
				double timeCost = 0;
				double length = 0;
				for (JGraptTEdgeWrapper j : shortestPath) {
					timeCost += j.getEdge().getTimeCost();
					length += j.getEdge().getLength();
					edges.add(j.getEdge());
				}
				
				n = new SPEntry(edges, timeCost, length);
			} else { // no path exists
				n = new SPEntry();
			}
			
			m.put(toID, n);
			
			return n;
		}
		
		private class SPEntry {
			/**
			 * this is to keep track of entries with no path, but currently isn't used (the default 
			 * values for "no path" being set in the constructor)
			 */
			@SuppressWarnings("unused")
			private boolean noPath;
			
			private List<Edge> shortestPath;
			private double shortestPathTimeCost;
			private double shortestPathLength;
			
			public SPEntry(List<Edge> shortestPath, double shortestPathTimeCost, double shortestPathLength) {
				this.shortestPath = shortestPath;
				this.shortestPathTimeCost = shortestPathTimeCost;
				this.shortestPathLength = shortestPathLength;
				this.noPath = false;
			}
			
			/**entry representing no path*/
			public SPEntry() {
				this.shortestPath = null;
				this.shortestPathTimeCost = Double.POSITIVE_INFINITY;
				this.shortestPathLength = Double.POSITIVE_INFINITY;
				this.noPath = true;
			}
			
			public List<Edge> getShortestPath() {
				return shortestPath;
			}
			
			public double getShortestPathTimeCost() {
				return shortestPathTimeCost;
			}
			
			public double getShortestPathLength() {
				return shortestPathLength;
			}
		}
	}
	
	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
//		Random r = new Random(10);
		Graph g = new Graph();
		Problem p = Problem.loadFromFile("resources/postpac3-withjobs.json");
		//Problem p = Problem.loadFromFile("/postpac2.json");
		//Problem p = Problem.loadFromFile("/intermediate-withtrucks.json");
		//Problem p = Problem.loadFromFile("/intermediate-withallocations.json");
		//Problem p = Problem.loadFromFile("/postpac-withtrucks.json");

		g.prepare(p);
		
//		System.out.println(g.renderGraphViz());
		
//		for (Node n : g.getNodes()) {
//			System.out.println(n + "{");
//			for (List<Resource> lr : n.getResources().values()) {
//				System.out.println("  " + lr);
//				for (Resource r : lr) {
//					System.out.println("    " + r);
//				}
//			}
//			System.out.println("}");
//		}
		
		g.renderGraphInJGraphX("postpac3.png");
	}
}
