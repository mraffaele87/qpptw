package se.dataductus.postpac.warehouse.solution.qpptw;

import java.util.ArrayList;
import java.util.List;

import se.dataductus.postpac.warehouse.solution.ItemTrip;

/**Wraps up a route for a truck*/
public class Route {
	private List<RouteStep> steps;
	private List<NodeVisit> nodeVisits;
	private List<QPPTWEdge> edges;
	private double length;
	
	private double startTimeInSeconds;
	private double endTimeInSeconds;
	private ItemTrip itemTrip;
	
	public Route(List<RouteStep> steps, List<NodeVisit> nodeVisits) {
		this.steps = steps;
		this.nodeVisits = nodeVisits;
		this.length = 0;
		
		this.edges = new ArrayList<>(steps.size());
		for (int i = 0; i < steps.size(); i++) {
			if (steps.get(i) instanceof RouteEdgeReservation) {
				this.edges.add(((RouteEdgeReservation)(steps.get(i))).getEdge());
				this.length += ((RouteEdgeReservation)(steps.get(i))).getEdge().getLength();
			}
		}
		
		this.itemTrip = null;
		
		if (!nodeVisits.isEmpty()) {
			this.startTimeInSeconds = nodeVisits.get(0).getTimePeriod().getA(); // the earliest we can leave at the start
			this.endTimeInSeconds = nodeVisits.get(nodeVisits.size() - 1).getTimePeriod().getA(); // the earliest we can arrive at the end
		}
	}
	
	public void addToEnd(Route secondRoute) {
		this.steps.addAll(secondRoute.getSteps());
		this.nodeVisits.addAll(secondRoute.getNodeVisits());
		this.edges.addAll(secondRoute.getEdges());
		this.length += secondRoute.getLength();
		this.endTimeInSeconds = secondRoute.getEndTimeInSeconds();
	}
	
	public void setItemTrip(ItemTrip itemTrip) {
		this.itemTrip = itemTrip;
	}
	
	public ItemTrip getItemTrip() {
		return itemTrip;
	}
	
	public double getStartTimeInSeconds() {
		return startTimeInSeconds;
	}
	
	public double getEndTimeInSeconds() {
		return endTimeInSeconds;
	}
	
	public double getLength() {
		return length;
	}
	
	public List<QPPTWEdge> getEdges() {
		return edges;
	}

	public List<RouteStep> getSteps() {
		return steps;
	}
	
	public List<NodeVisit> getNodeVisits() {
		return nodeVisits;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer("Route[");
		boolean first = true;
		
		for (RouteStep rs : this.steps) {
			if (first) {
				first = false;
			} else {
				buf.append(" > ");
			}
			
			buf.append(rs.toString());
		}
		buf.append("]");
		
		return buf.toString();
	}
	
	/**
	 * describes a visit through a node by a moving truck
	 * node - the node visited
	 * timePeriod - the time range during which the truck may pass this node
	 * */
	public static class NodeVisit {
		private QPPTWNode node;
		private TimePeriod timePeriod;
		
		public NodeVisit(QPPTWNode node, TimePeriod timePeriod) {
			this.node = node;
			this.timePeriod = timePeriod;
		}
		
		public QPPTWNode getNode() {
			return node;
		}
		
		/**the time period is a range of times that the truck could pass through the node: A is the time of arrival, B is the latest possible time of leaving*/
		public TimePeriod getTimePeriod() {
			return timePeriod;
		}
		
		@Override
		public String toString() {
			return "NV[" + this.node.getUnderlyingNode().getId() + "|" + this.timePeriod.getA() + "]";
		}
	}
}
