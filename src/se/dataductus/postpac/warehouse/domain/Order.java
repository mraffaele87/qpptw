package se.dataductus.postpac.warehouse.domain;

import java.util.ArrayList;
import java.util.List;

public class Order {
	private final int id;
	private final List<OrderItem> items;
	
	public Order(int id, List<Item> items){
		this.id = id;
		this.items = groupItems(items);
	}
	
	/**
	 * get a list of items and their quantities required by this order
	 * there will not be more than one entry in this list for the same Item
	 */
	public List<OrderItem> getItems() {
		return items;
	}
	
	public int getId() {
		return id;
	}
	
	/**groups together identical items in the order*/
	private List<OrderItem> groupItems(List<Item> items) {
		items = new ArrayList<Item>(items); // create a new arraylist so we can manipulate
		List<OrderItem> rval = new ArrayList<OrderItem>();
		
		while (!items.isEmpty()) {
			Item item = items.remove(0);
			int quantity = 1;
			
			// look for duplicates
			for (int i = 0; i < items.size();) {
				if (item.equals(items.get(i))) {
					quantity++;
					items.remove(i);
				} else {
					i++;
				}
			}
			
			rval.add(new OrderItem(item, quantity));
		}
		
		return rval;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		boolean first = true;
		for (OrderItem item : this.items) {
			buf.append(first ? "" : ", ");
			buf.append(item);
			first = false;
		}
		
		return "Order[" + buf.toString() + "]";
	}
}
