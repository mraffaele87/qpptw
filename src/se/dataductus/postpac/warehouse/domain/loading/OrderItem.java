package se.dataductus.postpac.warehouse.domain.loading;

/**wraps up an Item and the quantity of it required by this Order*/
public class OrderItem {
	private int itemID;
	private int quantity;
	
	public OrderItem(int itemID, int quantity) {
		this.itemID = itemID;
		this.quantity = quantity;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public String toString() {
		return "OrderItem[item=" + this.itemID + ", qty=" + this.quantity + "]";
	}
}