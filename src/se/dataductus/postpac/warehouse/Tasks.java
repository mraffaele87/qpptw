package se.dataductus.postpac.warehouse;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import se.dataductus.postpac.warehouse.domain.Item;
import se.dataductus.postpac.warehouse.domain.Order;
import se.dataductus.postpac.warehouse.domain.OrderItem;
import se.dataductus.postpac.warehouse.domain.loading.Problem;

/**
 * Generates an instance of the problem, that is, a list of orders to be fulfilled
 * Call randomizeJobs() to initialise the problem. getJobList() does what might be reasonably expected.
 */
public class Tasks {
	
	public List<Order> orders;
	private Random random;
	
	public Tasks(Random random){
		this.random = random;
		this.orders = new ArrayList<>();
	}
	
	public Tasks(Random random, Problem p, Graph g){
		this.random = random;
		this.orders = new ArrayList<>();
		this.readFromProblem(p, g);
	}
	
	public void randomizeJobs(Graph graph, int itemsPerOrder, int numOrders){
		int numItems = graph.getItems().size();
		
		for(int i=0;i<numOrders;i++){
			List<Item> items = new ArrayList<Item>();
			for(int j=0;j<itemsPerOrder;j++){
				
				int m = this.random.nextInt(numItems);
				
				items.add(graph.getItems().get(m));
			}
			Order o = new Order(i, items);
			orders.add(o);
		}
	}
	
	/**@return a reference to the provided problem*/
	public Problem renderToProblem(Problem problem) {
		List<se.dataductus.postpac.warehouse.domain.loading.Order> orders = new ArrayList<>();
		for (Order order : this.orders) {
			List<se.dataductus.postpac.warehouse.domain.loading.OrderItem> orderItems = new ArrayList<>();
			for (OrderItem orderItem : order.getItems()) {
				orderItems.add(new se.dataductus.postpac.warehouse.domain.loading.OrderItem(orderItem.getItem().getId(), orderItem.getQuantity()));
			}
			orders.add(new se.dataductus.postpac.warehouse.domain.loading.Order(order.getId(), orderItems));
		}
		
		problem.setOrders(orders);
		
		return problem;
	}
	
	/**Graph is supplied so we can reuse Item objects from it*/
	private void readFromProblem(Problem problem, Graph graph) {
		for (se.dataductus.postpac.warehouse.domain.loading.Order order : problem.getOrders()) {
			List<Item> items = new ArrayList<Item>(order.getItems().size());
			for (se.dataductus.postpac.warehouse.domain.loading.OrderItem oi : order.getItems()) {
				Item item = graph.getItems().get(oi.getItemID());
				
				for (int i = 0; i < oi.getQuantity(); i++) {
					items.add(item);
				}
			}
			
			this.orders.add(new Order(order.getId(), items));
		}
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	
	public Order getOrderById(int id) {
		Order o = orders.get(id);
		if (o.getId() == id) {
			return o;
		} else {
			for (Order o2 : orders) {
				if (o2.getId() == id) {
					return o2;
				}
			}
			return null;
		}
	}
}
