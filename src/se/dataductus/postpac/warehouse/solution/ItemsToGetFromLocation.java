package se.dataductus.postpac.warehouse.solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.dataductus.postpac.warehouse.domain.Item;
import se.dataductus.postpac.warehouse.domain.Resource;

/**wraps up identical items from different orders to get from a specific resource*/
public class ItemsToGetFromLocation {
	private final List<ItemToGet> itemsToGet;
	private final Resource location;
	private final Item item;
	private final int quantity;
	
	/**@throws IllegalArgumentException if any of the items to get are for different item IDs, from different locations, or if no items are specified*/
	public ItemsToGetFromLocation(List<ItemToGet> itemsToGet) {
		if (itemsToGet.isEmpty()) {
			throw new IllegalArgumentException("itemsToGet is empty!");
		}
		
		this.item = itemsToGet.get(0).getItem();
		this.location = itemsToGet.get(0).getLocation();
		
		int quantity = 0;
		
		for (ItemToGet itemToGet : itemsToGet) {
			if (!itemToGet.getItem().equals(item)) {
				throw new IllegalArgumentException("Items must all be the same! Found at least " + itemToGet.getItem() + " and " + item);
			}
			if (!itemToGet.getLocation().equals(location)) {
				throw new IllegalArgumentException("Items must all be from the same location! Found at least " + itemToGet.getLocation() + " and " + location);
			}
			
			quantity += itemToGet.getQuantity();
		}
		this.quantity = quantity;
		
		this.itemsToGet = new ArrayList<>(itemsToGet);
	}
	
	public List<ItemToGet> getItemsToGet() {
		return Collections.unmodifiableList(itemsToGet);
	}

	public Resource getLocation() {
		return location;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public Item getItem() {
		return item;
	}
	
	public double getTimeCostForLoading() {
		return quantity * location.getTimeCost();
	}
}
