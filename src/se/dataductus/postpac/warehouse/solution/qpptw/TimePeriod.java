package se.dataductus.postpac.warehouse.solution.qpptw;

public class TimePeriod {
	private final double a;
	private final double b;
	
	public TimePeriod(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double getA() {
		return a;
	}
	
	public double getB() {
		return b;
	}
	
	public boolean containsTime(double time) {
		return (time >= a) && (time <= b);
	}
	
	@Override
	public String toString() {
		return "TP[" + a + "-" + b + "]";
	}
}