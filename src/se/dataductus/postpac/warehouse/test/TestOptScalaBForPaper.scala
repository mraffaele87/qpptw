package se.dataductus.postpac.warehouse.test

import java.util.Random
import haiku.localsearch.Locality
import haiku.localsearch.Minimise
import haiku.localsearch.Search
import haiku.localsearch.combinators.SA
import se.dataductus.postpac.warehouse.Graph
import se.dataductus.postpac.warehouse.Tasks
import se.dataductus.postpac.warehouse.domain.loading.Problem
import se.dataductus.postpac.warehouse.solution.SubProblemASolution
import se.dataductus.postpac.warehouse.solution.SubProblemBSolution

object TestOptScalaBForPaper {
  
  // general setting up
  val r = new Random
  val p : Problem = Problem.loadFromFile(new java.io.File("./resources/postpac2-withtrips-longertimes.json"))
  val g : Graph   = new Graph(p)
  val t : Tasks   = new Tasks(r, p, g)
  val loadedSolA : SubProblemASolution = SubProblemASolution.readFromProblem(p, g, t, r);
  val iterations = 1000
  
  // some stats for the paper
  //println("avg items per trip " + p.averageItemsPerTrip())
  println("number of trips " + p.getItemTrips.size())
  println("iterations " + iterations)
  println("number of nodes " + p.getNodes.size())
  println("number of edges " + p.getEdges.size())
  
  // we need three ingredients to construct a search:
  
  // 1. starting solution
  //val seed : SubProblemBSolution = SubProblemBSolution.generate(g, loadedSolA, r);
  
  // 2. neighbourhood function (what could be the next solution)
  // Locality.stepFn creates a neighbourhood function from a mutation operator
  // don't forget to include the Ctx!
  def step[Ctx] = Locality.stepFn[SubProblemBSolution, Ctx] { (x : SubProblemBSolution) => x.mutate }
  //                              ^^^^^^^^^^^^^^^^^^^         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  
  // 3. bias (fitness) function (how you select the next solution)
  // Minimise creates a minimising fitness function from any function on an ordered domain
  def fitness[Ctx] = Minimise[SubProblemBSolution, Double, Ctx] { x => x.getFitness }
  //                          ^^^^^^^^^^^^^^^^^^^                 ^^^^^^^^^^^^^^^^^

  
  // if you have the three ingredients, it's straightforward to create the search object
  // the Ctx should be Unit (which means empty context)
  //val search : Search[SubProblemBSolution,Unit] = new Search(seed, step, fitness)
  //                  ^^^^^^^^^^^^^^^^^^^                    ^^^^^^^^^^^^^^^^^^^
  
  
  // this is how you transform/decorate a search with Simulated Annealing
  // 1. create the decorator
  def sadecorator[Ctx] = SA.MetropolisHastings[SubProblemBSolution,Ctx]( 100, (x => 0.95*x) , fitness )
  //                                                                     ^ starting temp.
  //                                                                          ^ annealing schedule
  //                                                                                          ^ fitness fn.
  //val sasearch = sadecorator transforms search
    // syntactically the same as ~~: sadecorator.transforms(search)
  
  def main( args : Array[String] ) : Unit = {
    println("Initialised");
    println("i,seedTime,seedFitness,hcTime,hcFitness,saTime,saFitness")
    
    for (i <- 1 to 30) {
      val r = new Random(i)
      val startTimeRnd = compat.Platform.currentTime
      val seed : SubProblemBSolution = SubProblemBSolution.generate(g, loadedSolA, r, true);
      val seedFitness = (fitness function seed)
      val rndTime = compat.Platform.currentTime - startTimeRnd
      
      val startTimeHC = compat.Platform.currentTime
      
      // this is how you run a search and get the result
      val search : Search[SubProblemBSolution,Unit] = new Search(seed, step, fitness)
      val result = search.run(iterations)
      //println("Hill climbing complete");
      //println((compat.Platform.currentTime - startTimeHC) + "ms")
      val hcTime = compat.Platform.currentTime - startTimeHC
      val hcFitness = fitness function result
      
      val startTimeSA = compat.Platform.currentTime
      
      val sasearch = sadecorator transforms search
      val saresult = sasearch.run(iterations)
      //println("SA complete");
      //println((compat.Platform.currentTime - startTimeSA) + "ms")
      val saTime = compat.Platform.currentTime - startTimeSA
      val saFitness = fitness function saresult
      
      //println( "Seed fitness: ")
      
      //println("Hill climbing: ")
      //println( result )
      //println( fitness function result )
      
      //println("")
      //println("Simulated annealing: ")
      //println( saresult )
      //println( fitness function saresult )
      println(i + "," + rndTime + "," + seedFitness + "," + hcTime + "," + hcFitness + "," + saTime + "," + saFitness)
    }  
//    if ((fitness function result) < (fitness function saresult)) {
//      result.copyRoutesToTrucks();
//    } else {
//      saresult.copyRoutesToTrucks();
//    }
    
    // visualise allocated movements
    //g.renderGraphInJGraphX(0, 20, 0.05, 50);
    
    // write solution to JSON
//    g.renderTrucksToProblem(p);
//    p.writeToFile("resources/postpac2-solution.json")
    println("All done.");
  }
}